<?php
/**
 * Created by PhpStorm.
 * User: davidsjoo
 * Date: 15-09-23
 * Time: 11:58
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Messages;
use AppBundle\Form\Type\MessagesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Doctrine\UserManager;

class MessagesController extends Controller
{
    /**
     * @Route("/messages", name="messages")
     */
    public function messagesIndex() {

        $repository = $this->getDoctrine()->getRepository('AppBundle:Messages');
        $messages = $repository->findAllMessages();

        return $this->render('/messages/messages.html.twig', array(
            'messages' => $messages,
        ));
    }

    /**
     * @Route("/messages/new_message", name="new_message")
     */
    public function newAction(Request $request)
    {
        $message = new Messages();
        $form = $this->createForm(new MessagesType(), $message);
        $form->handleRequest($request);
        $repository = $this->getDoctrine()->getRepository('AppBundle:Messages');
        $all_messages = $repository->findAllMessages();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $message->setDate(new \DateTime('now'));
            
            $message->setUser($this->getUser());
            $em->persist($message);
            $em->flush();
        
            return $this->redirectToRoute('messages');
        }

        return $this->render('messages/new_message.html.twig', array(
            'form' => $form->createView(),
            'all_messages' => $all_messages,
        ));
    }

    /**
     * @Route("/message/update_message/{id}")
     */
    public function updateMessage(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository('AppBundle:Messages')->find($id);
        $form = $this->createForm(new MessagesType(), $message);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em->persist($message);
            $em->flush();

            return $this->redirectToRoute('messages');
        }

        return $this->render('/messages/update_message.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/messages/delete_message/{id}")
     */
    public function deleteMessage($id) {

        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository('AppBundle:Messages')->find($id);
        $em->remove($message);
        $em->flush();

        return $this->redirect('/messages');
    }

    /**
     * @Route("/messages/show_message/{id}")
     * @Template("/messages/show_message.html.twig")
     */
    public function showMessage($id) {

        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository('AppBundle:Messages')->find($id);

        return array(
            'message' => $message,
        );
    }








}