<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class HemController extends Controller
{
    /**
     * @Route("/hem", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $board_repository = $em->getRepository('AppBundle:Messageboard');
        $board_message = $board_repository->findAll();

        $messages_repository = $em->getRepository('AppBundle:Messages');

        $latest_message = $messages_repository->getLatestMessage();
        $last_five_messages= $messages_repository->getLatestFiveMessage();
        $today = new \DateTime('today');
        $todays_gamedays = $em->getRepository('AppBundle:GameDay')->findTodaysGameDays($today);
        $user = $this->getUser();

        $trades = $em->getRepository('AppBundle:TradeGamebooking')->findTradesHome();
        $gameday_trades = $em->getRepository('AppBundle:TradeGamedayBooking')->findGamedayTradesHome();

        return $this->render('default/hem.html.twig', array(
            'trades' => $trades,
            'user' => $user,
            'todays_gamedays' => $todays_gamedays,
            'board_message' => $board_message,
            'latest_message' => $latest_message,
            'latest_five' => $last_five_messages,
            'gameday_trades' => $gameday_trades,
        ));
    }


}
