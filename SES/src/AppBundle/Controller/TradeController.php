<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TradeGamebooking;
use AppBundle\Form\Type\TradeGamebookingType;
use AppBundle\Form\Type\AcceptGamebookingTradeType;
use AppBundle\Form\Type\ApprovedGamebookingTradeType;
use AppBundle\Entity\TradeGameBookingRepository;
use AppBundle\Entity\TradeGamedayBookingRepository;
use AppBundle\Entity\TradeGamedayBooking;

use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class TradeController extends Controller
{
	/**
	 * @Route("/trade", name="trade")
	 * @Template("/trade/trade.html.twig")
	 */
	public function showTrades()
	{
	    $trades = $this->getDoctrine()->getRepository('AppBundle:TradeGamebooking')->findTrades();
	    $gameday_trades = $this->getDoctrine()->getRepository('AppBundle:TradeGamedayBooking')->findTrades();

	    return array('trades' => $trades, 'gameday_trades' => $gameday_trades,);
	}

	/**
	 * @Route("/trade/accepted_trades", name="accepted_trades")
	 * @Template("/trade/accepted_trades.html.twig")
	 */
	public function showAcceptedTrades()
	{
	    $trades = $this->getDoctrine()->getRepository('AppBundle:TradeGamebooking')->findAcceptedTrades();
	    $gameday_trades = $this->getDoctrine()->getRepository('AppBundle:TradeGamedayBooking')->findAcceptedTrades();

	    return array('trades' => $trades, 'gameday_trades' => $gameday_trades,);
	}

	/**
	 * @Route("/trade/approved_trades", name="approved_trades")
	 * @Template("/trade/approved_trades.html.twig")
	 */
	public function showApprovedTrades()
	{
	    $trades = $this->getDoctrine()->getRepository('AppBundle:TradeGamebooking')->findApprovedTrades();
	    $gameday_trades = $this->getDoctrine()->getRepository('AppBundle:TradeGamedayBooking')->findApprovedTrades();

	    return array('trades' => $trades, 'gameday_trades' => $gameday_trades,);
	}

	/**
	 * @Route("/trade/new/{gamebooking_id}", name="new_trade")
	 * @Template("/trade/new_trade.html.twig")
	 */
	public function newGamebookingTrade(Request $request, $gamebooking_id)
	{
		$em = $this->getDoctrine()->getManager();
	    $trade = new TradeGamebooking();
	    $gamebooking = $em->getRepository('AppBundle:GameBooking')->find($gamebooking_id);
	    $form = $this->createForm(new TradeGamebookingType(), $trade);
	    $form->handleRequest($request);

	    if ($form->isValid()) {
	    	$trade->setTradeCreatedDate(new \DateTime('now'));
	    	$trade->setTradeApproved(false);
	    	$trade->setGamebooking($gamebooking);
	    	$trade->setTradeUser($this->getUser());
	    	$em->persist($trade);
	    	$em->flush();

	    	if ($request->isXmlHttpRequest()) {
	    		$response = array('success' => true,);
	    		return new JsonResponse($response);
	    	}
	    }

	    return array('trade' => $trade, 'form' => $form->createView(), 'gamebooking' => $gamebooking,);
	}

	/**
	 * @Route("/trade/accept/{id}", name="accept_trade")
	 * @Template("/trade/accept_trade.html.twig")
	 */
	public function acceptGameBookingTrade(Request $request, $id)
	{
	    $em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGamebooking')->find($id);
	    $form = $this->createForm(new AcceptGamebookingTradeType(), $trade);
	    $form->handleRequest($request);

	    if ($form->isValid()) {
	    	$mailer = $this->get('mailer');
	    	$message = \Swift_Message::newInstance()
            ->setSubject('VIKTIGT!!!')
            ->setFrom('sesmailtest@gmail.com')
            ->setTo('sesmailtest@gmail.com')
            ->setBody($this->renderView('/trade/email.txt.twig', array('test' => 'test'
                )));
            $mailer->send($message);
	    	$trade->setTradeAcceptedUser($this->getUser());
	    	$em->flush();
    	    

	    	if ($request->isXmlHttpRequest()) {
	    		$response = array('success' => true,);
	    		return new JsonResponse($response);
	    	}
	    }

	    return array('trade' => $trade, 'form' => $form->createView(),);
	}

	/**
	 * @Route("/trade/approve/{id}", name="approve_trade")
	 * @Template("/trade/approve_trade.html.twig")
	 */
	public function approveGameBookingTrade(Request $request, $id)
	{
	    $em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGamebooking')->find($id);
	    $form = $this->createForm(new ApprovedGamebookingTradeType(), $trade);
	    $game_booking = $em->getRepository('AppBundle:GameBooking')->find($trade->getGameBooking()->getId());
	    $form->handleRequest($request);

	    if ($form->isValid()) {
	    	$game_booking->setUser($trade->getTradeAcceptedUser());
	    	$trade->setTradeApprovedDate(new \DateTime('now'));
	    	$trade->settradeApprovedUser($this->getUser());
	    	$em->flush();

	    	if ($request->isXmlHttpRequest()) {
	    		$response = array('success' => true, 'booking_id' => $game_booking->getId(),);
	    		return new JsonResponse($response);
	    	}
	    }

	    return array('trade' => $trade, 'form' => $form->createView(),);
	}

	/**
	 * @Route("/trade/new_gameday/{gameday_booking_id}", name="new_gameday_trade")
	 * @Template("/trade/new_gameday_trade.html.twig")
	 */
	public function newGamedaybookingTrade(Request $request, $gameday_booking_id)
	{
		$em = $this->getDoctrine()->getManager();
	    $trade = new TradeGamedayBooking();
	    $gameday_booking = $em->getRepository('AppBundle:GameDayBooking')->find($gameday_booking_id);
	    $form = $this->createForm(new TradeGamebookingType(), $trade);
	    $form->handleRequest($request);

	    if ($form->isValid()) {
	    	$trade->setTradeCreated(new \DateTime('now'));
	    	$trade->setTradeApproved(false);
	    	$trade->setGamedayBooking($gameday_booking);
	    	$trade->setTradeUser($this->getUser());
	    	$em->persist($trade);
	    	$em->flush();

	    	if ($request->isXmlHttpRequest()) {
	    		$response = array('success' => true,);
	    		return new JsonResponse($response);
	    	}
	    }

	    return array('trade' => $trade, 'form' => $form->createView(), 'gameday_booking' => $gameday_booking,);
	}

	/**
	 * @Route("/trade/accept_gameday/{id}", name="accept_gameday_trade")
	 * @Template("/trade/accept_gameday_trade.html.twig")
	 */
	public function acceptGamedayBookingTrade(Request $request, $id)
	{
	    $em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGamedayBooking')->find($id);
	    $form = $this->createForm(new AcceptGamebookingTradeType(), $trade);
	    $form->handleRequest($request);

	    if ($form->isValid()) {
	    	$mailer = $this->get('mailer');
	    	$message = \Swift_Message::newInstance()
            ->setSubject('VIKTIGT!!!')
            ->setFrom('sesmailtest@gmail.com')
            ->setTo('sesmailtest@gmail.com')
            ->setBody($this->renderView('/trade/email.txt.twig', array('test' => 'test'
                )));
            $mailer->send($message);
	    	$trade->setTradeAcceptedUser($this->getUser());
	    	$em->flush();

	    	if ($request->isXmlHttpRequest()) {
	    		$response = array('success' => true,);
	    		return new JsonResponse($response);
	    	}
	    }

	    return array('trade' => $trade, 'form' => $form->createView(),);
	}

	/**
	 * @Route("/trade/approve_gameday/{id}", name="approve_gameday_trade")
	 * @Template("/trade/approve_gameday_trade.html.twig")
	 */
	public function approveGamedayBookingTrade(Request $request, $id)
	{
	    $em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGamedayBooking')->find($id);
	    $form = $this->createForm(new ApprovedGamebookingTradeType(), $trade);
	    $gameday_booking = $em->getRepository('AppBundle:GameDayBooking')->find($trade->getGamedayBooking()->getId());
	    $form->handleRequest($request);

	    if ($form->isValid()) {
	    	$gameday_booking->setUser($trade->getTradeAcceptedUser());
	    	$trade->setTradeApprovedDate(new \DateTime('now'));
	    	$trade->settradeApprovedUser($this->getUser());
	    	$em->flush();

	    	if ($request->isXmlHttpRequest()) {
	    		$response = array('success' => true, 'booking_id' => $gameday_booking->getId(),);
	    		return new JsonResponse($response);
	    	}
	    }

	    return array('trade' => $trade, 'form' => $form->createView(),);
	}

	/**
	 * @Route("/trade/game/remove/{id}", name="accept_remove_game_trade")
	 * @Template("/trade/remove_game_trade.html.twig")
	 */
	public function removeGameTradeAction(Request $request, $id)
	{
	    $em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGameBooking')->find($id);
	    return array('trade' => $trade);
	}

	/**
	 * @Route("/trade/game/{id}/remove", name="remove_game_trade")
	 * @Template()
	 */
	public function deleteGameTradeAction(Request $request, $id)
	{
	    $em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGameBooking')->find($id);
	    $em->remove($trade);
	    $em->flush();
	    if ($request->isXmlHttpRequest()) {
            $response = array(
                'success' => true,
            );
            return new JsonResponse($response);
        }
	}

	/**
	 * @Route("/trade/game/undo/{id}", name="undo_game_trade")
	 * @Template("/trade/undo_game_trade.html.twig")
	 */
	public function undoGameTradeAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGameBooking')->find($id);
	    return array('trade' => $trade);
	}

	/**
	 * @Route("/trade/game/undo/{id}/save", name="undo_game_trade_save")
	 * @Template()
	 */
	public function undoGameTradeSaveAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGameBooking')->find($id);
	    $trade->setTradeAcceptedUser(null);
	    $em->flush();
	    if ($request->isXmlHttpRequest()) {
            $response = array(
                'success' => true,
            );
            return new JsonResponse($response);
        }
	}

	/**
	 * @Route("/trade/gameday/remove/{id}", name="accept_remove_gameday_trade")
	 * @Template("/trade/remove_gameday_trade.html.twig")
	 */
	public function removeGamedayTradeAction(Request $request, $id)
	{
	    $em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGameDayBooking')->find($id);
	    return array('trade' => $trade);
	}

	/**
	 * @Route("/trade/gameday/{id}/remove", name="remove_gameday_trade")
	 * @Template()
	 */
	public function deleteGamedayTradeAction(Request $request, $id)
	{
	    $em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGameDayBooking')->find($id);
	    $em->remove($trade);
	    $em->flush();
	    if ($request->isXmlHttpRequest()) {
            $response = array(
                'success' => true,
            );
            return new JsonResponse($response);
        }
	}

	/**
	 * @Route("/trade/gameday/undo/{id}", name="undo_gameday_trade")
	 * @Template("/trade/undo_gameday_trade.html.twig")
	 */
	public function undoGamedayTradeAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGameDayBooking')->find($id);
	    return array('trade' => $trade);
	}

	/**
	 * @Route("/trade/gameday/undo/{id}/save", name="undo_gameday_trade_save")
	 * @Template()
	 */
	public function undoGamedayTradeSaveAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();
	    $trade = $em->getRepository('AppBundle:TradeGameDayBooking')->find($id);
	    $trade->setTradeAcceptedUser(null);
	    $em->flush();
	    if ($request->isXmlHttpRequest()) {
            $response = array(
                'success' => true,
            );
            return new JsonResponse($response);
        }
	}

}