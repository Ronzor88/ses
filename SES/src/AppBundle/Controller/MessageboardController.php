<?php
/**
 * Created by PhpStorm.
 * User: davidsjoo
 * Date: 15-09-21
 * Time: 12:57
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MessageboardController extends Controller
{

    /**
     * @Route("/anslag/{id}", name="messageboard")
     */

    public function showBoard(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $board_message = $em->getRepository('AppBundle:Messageboard')->find($id);

        if (!$board_message) {
            throw $this->createNotFoundException('There is nothing here..');
        }

        $form = $this->createFormBuilder($board_message)
            ->add('boardMessage', 'ckeditor', array(
                'required' => false,
                'attr' => array('class'=>'board_message','placeholder' => 'Skriv ditt meddelande här...'),
                'label' => false,
                'ui_color' => '#f4f2f0',
                'width'         => '100%',
                'height'        => '420',


            ))




            ->getForm();
        $form->handleRequest($request);





        if ($form->isValid()) {
            $board_message->setUser($this->getUser());
            $em->persist($board_message);
            $em->flush();
            return $this->redirectToRoute('homepage');

        }

        return $this->render('/default/anslagstavla.html.twig', array(
            'board_message' => $board_message,
            'form' => $form->createView()





        ));
    }

}