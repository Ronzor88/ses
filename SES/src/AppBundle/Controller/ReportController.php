<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Type;
use AppBundle\Entity\Games;
use AppBundle\Entity\GameDay;
use AppBundle\Entity\GamesTypes;
use AppBundle\Entity\GameDayTypes;
use AppBundle\Entity\GameBooking;
use AppBundle\Entity\GameDayBooking;
use AppBundle\Form\Type\ReportType;

use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ReportController extends Controller
{
    /**
     * @Route("/report", name="report")
     * @Template("/report/report.html.twig")
     */
    public function showReport(Request $request)
    {
        $form = $this->createForm(new ReportType());
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $gameday_bookings_repository = $em->getRepository('AppBundle:GameDayBooking');
        $game_bookings_repository = $em->getRepository('AppBundle:GameBooking');

        if ($form->isValid()) {
            $start_date = $form["start_date"]->getData();
            $end_date = $form["end_date"]->getData();
            $user = $form["user"]->getData();
            $gameday_bookings = $gameday_bookings_repository->findGamedayBookingByUserAndDateJson($user, $start_date, $end_date);
            $game_bookings = $game_bookings_repository->findGameBookingByUserAndDateJson($user, $start_date, $end_date);

            $count_gamebookings = array();
            $count_gamedaybookings = array();
            $cmh = 0;
            $livered = 0;
            $fls = 0;
            $wf = 0;
            $highlights = 0;
            $other = 0;
            $game_other = 0;

            $gameday_types = array();
            $game_types = array();

            foreach ($gameday_bookings as $booking) {
                switch ($booking->getType()->getName()) {
                    case "CMH":
                        $cmh++;
                        break;
                    case "Livered":
                        $livered++;
                        break;
                    case "FLS":
                        $fls++;
                        break;
                    case "WF";
                        $wf++;
                        break;
                    default:
                        $other++;
                        break;
                }
            }

            foreach ($game_bookings as $game_booking) {
                if ($game_booking->getType()->getName()) {
                    $highlights++;
                }
                else {
                    $game_other++;
                }
            }
            array_push($game_types, array('highlights' => $highlights, 'other' => $game_other));
            array_push($gameday_types, array('cmh' => $cmh, 'livered' => $livered, 'fls' => $fls, 'wf' => $wf, 'other' => $other, ));
            return array(
                'form' => $form->createView(), 
                'test' => "hej", 
                'start_date' => $start_date, 
                'gameday_bookings' => $gameday_bookings,
                'game_bookings' => $game_bookings,
                'count_gamedaybookings' => $count_gamedaybookings,
                'gameday_types' => $gameday_types,
                'game_types' => $game_types,
                
            );
        }
        return array('form' => $form->createView(),);
    }

    // Det nedanför här används inte, men sparade det för att kunna kopiera delar av koden.

	/**
     * @Route("/report/gameday", name="report_gameday")
     */
    public function reportGamdedayAction() {

        $request = $this->container->get('request');
        $user = $request->query->get('user');
        $start_date = $request->query->get('start_date');
        $end_date = $request->query->get('end_date');
        $em = $this->getDoctrine()->getManager();
        $testgame_repository = $em->getRepository('AppBundle:GameDayBooking');
        $gameday_bookings = $testgame_repository->findGamedayBookingByUserAndDateJson($user, $start_date, $end_date);
        $test = array();
        foreach ($gameday_bookings as $game) {
        	array_push($test, array('type' => $game->getType()->getName(), 'date' => $game->getGameDay()->getDate()->format('Y-m-d')));
        }

        return new JsonResponse($test);
    }

    /**
     * @Route("/report/gameday/count", name="report_gameday_count")
     */
    public function reportGamdedayCountAction() {

        $request = $this->container->get('request');
        $user = $request->query->get('user');
        $start_date = $request->query->get('start_date');
        $end_date = $request->query->get('end_date');
        $em = $this->getDoctrine()->getManager();
        $testgame_repository = $em->getRepository('AppBundle:GameDayBooking');
        $gameday_bookings = $testgame_repository->findGamedayBookingByUserAndDateCountJson($user, $start_date, $end_date);
        $test = array();
        foreach ($gameday_bookings as $game) {
            array_push($test, array('type' => $game->getType()->getName(), 'date' => $game->getGameDay()->getDate()->format('Y-m-d')));
        }

        return new JsonResponse($test);
    }

    /**
     * @Route("/report/game", name="report_game")
     */
    public function reportGameAction() {

        $request = $this->container->get('request');
        $user = $request->query->get('user');
        $start_date = $request->query->get('start_date');
        $end_date = $request->query->get('end_date');
        $em = $this->getDoctrine()->getManager();
        $gamebooking_repository = $em->getRepository('AppBundle:GameBooking');
        $game_bookings = $gamebooking_repository->findGameBookingByUserAndDateJson($user, $start_date, $end_date);
        $games = array();
        foreach ($game_bookings as $game) {
            array_push($games, array('type' => $game->getType()->getName(), 'date' => $game->getGame()->getDatetime()->format('Y-m-d')));
        }

        return new JsonResponse($games);
    }

    /**
     * @Route("/report/game/count", name="report_game_count")
     */
    public function reportGameCountAction() {

        $request = $this->container->get('request');
        $user = $request->query->get('user');
        $start_date = $request->query->get('start_date');
        $end_date = $request->query->get('end_date');
        $em = $this->getDoctrine()->getManager();
        $gamebooking_repository = $em->getRepository('AppBundle:GameBooking');
        $game_bookings = $gamebooking_repository->findGameBookingByUserAndDateCountJson($user, $start_date, $end_date);
        $games = array();
        foreach ($game_bookings as $game) {
            array_push($games, array('type' => $game->getType()->getName(), 'date' => $game->getGame()->getDatetime()->format('Y-m-d')));
        }

        return new JsonResponse($games);
    }

}