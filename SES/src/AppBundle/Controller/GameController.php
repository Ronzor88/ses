<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Type;
use AppBundle\Entity\Games;
use AppBundle\Entity\GameDay;
use AppBundle\Entity\GamesTypes;
use AppBundle\Entity\GameDayTypes;
use AppBundle\Entity\GameBooking;
use AppBundle\Entity\GameDayBooking;
use AppBundle\Entity\GameDayTypesRepository;
use AppBundle\Form\Type\GameBookingType;
use AppBundle\Form\Type\UpdateGameBookingTestType;
use AppBundle\Form\Type\GameDayBookingType;
use AppBundle\Form\Type\GameGradeType;

use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class GameController extends Controller
{
    /**
    * @Route("/games", name="games")
    * @Template("games/index.html.twig")
    */
    public function showGamesAction() 
    {
        $today = new \DateTime('today');
        $gamedays = $this->getDoctrine()->getRepository('AppBundle:GameDay')->findAllGameDays();
        $future_gamedays = $this->getDoctrine()->getRepository('AppBundle:GameDay')->findFutureGamedays($today);
        $user = $this->getUser();
        return array(
            'user' => $user, 'today' => $today, 'future_gamedays' => $future_gamedays,
        );
    }

    /**
    * @Route("/played_games", name="played_games")
    * @Template("games/played_games.html.twig")
    */
    public function showPlayedGamesAction() 
    {
        $today = new \DateTime('today');        
        $played_gamedays = $this->getDoctrine()->getRepository('AppBundle:GameDay')->findPlayedGamedays($today);
        $user = $this->getUser();
        return array(
            'user' => $user, 'today' => $today, 'played_gamedays' => $played_gamedays,
        );
    }

    /**
     * @Route("/games/admin_games", name="admin_games")
     * @Template("/games/admin_games.html.twig")
     */
    public function adminGamesAction(Request $request)
    {
        $today = new \DateTime('today');
        $gamedays_repository = $this->getDoctrine()->getRepository('AppBundle:GameDay');
        $future_gamedays = $gamedays_repository->findFutureGamedays($today);
        $types = $this->getDoctrine()->getRepository('AppBundle:GameDayTypes')->findAll();
        $bookings = $this->getDoctrine()->getRepository('AppBundle:GameDayBooking')->findAll();
      
        return array(
            'future_gamedays' => $future_gamedays,
        );
    }

    /**
     * @Route("/games/admin_games/played_games", name="admin_played_games")
     * @Template("/games/admin_played_games.html.twig")
     */
    public function adminPlayedGamesAction(Request $request)
    {
        $today = new \DateTime('today');
        $gamedays_repository = $this->getDoctrine()->getRepository('AppBundle:GameDay');
        $played_gamedays = $gamedays_repository->findPlayedGamedays($today);
        return array(
            'played_gamedays' => $played_gamedays,
        );
    }

    /**
     * @Route("/games/admin_games/unbooked_games", name="admin_unbooked_games")
     * @Template("/games/games_without_booking.html.twig")
     */
    public function adminUnbookedGamesAction(Request $request)
    {
        $today = new \DateTime('today');
        $gamedays_repository = $this->getDoctrine()->getRepository('AppBundle:GameDay');
        $future_gamedays = $gamedays_repository->findFutureGamedays($today);
        return array(
            'future_gamedays' => $future_gamedays,
        );
    }

    /**
     * @Route("/games/admin_games/all_games", name="admin_all_games")
     * @Template("/games/admin_all_games.html.twig")
     */
    public function adminAllGamesAction()
    {
        $today = new \DateTime('today');
        $gamedays = $this->getDoctrine()->getRepository('AppBundle:GameDay')->findAll();
        return array(
            'gamedays' => $gamedays,
            'today' => $today,
        );
    }

    /**
     * @Route("/games/my_games", name="my_games")
     * @Template("/games/my_games.html.twig")
     */
    public function myGamesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $today = (new \DateTime('now'));
        $comming_gameday_bookings = $em->getRepository('AppBundle:GameDayBooking')->findAllMyCommingGameDayBookings($user, $today);        
        $gamebooking_repository = $em->getRepository('AppBundle:GameBooking'); 
        $comming_game_bookings = $gamebooking_repository->findAllMyCommingGameBookings($user, $today);            
        $game_bookings = $gamebooking_repository->findAllMyGameBookings($user);
        $games_without_grade = $em->getRepository('AppBundle:Games')->findCompletedGameBookingsWithoutGrade($user, $today);

        return array(
            'user' => $user, 
            'comming_gameday_bookings' => $comming_gameday_bookings, 
            'game_bookings' => $game_bookings,
            'comming_game_bookings' => $comming_game_bookings,
            'games_without_grade' => $games_without_grade,
        );
    }

    /**
     * @Route("/games/my_games/comming", name="my_comming_games")
     * @Template("/games/my_comming_games.html.twig")
     */
    public function myCommingGamesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $today = (new \DateTime('now'));
        $comming_gameday_bookings = $em->getRepository('AppBundle:GameDayBooking')->findAllMyCommingGameDayBookings($user, $today);
        $gamebooking_repository = $em->getRepository('AppBundle:GameBooking'); 
        $comming_game_bookings = $gamebooking_repository->findAllMyCommingGameBookings($user, $today);

        return array(
            'user' => $user,
            'comming_gameday_bookings' => $comming_gameday_bookings,
            'comming_game_bookings' => $comming_game_bookings,);
    }

    /**
     * @Route("/games/my_played_games", name="my_played_games")
     * @Template("/games/my_played_games.html.twig")
     */
    public function myPlayedGamesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $today = (new \DateTime('now'));
        $complete_gameday_bookings = $em->getRepository('AppBundle:GameDayBooking')->findAllMyCompletedGameDayBookings($user, $today);
        $gamebooking_repository = $em->getRepository('AppBundle:GameBooking'); 
        $complete_game_bookings = $gamebooking_repository->findAllMyCompletedGameBookings($user, $today);

        return array(
            'user' => $user,
            'complete_gameday_bookings' => $complete_gameday_bookings,
            'complete_game_bookings' => $complete_game_bookings,);
    }

    /**
     * @Route("/games/my_trades", name="my_trades")
     * @Template("/games/my_trades.html.twig")
     */
    public function myTradesAction()
    {
        $user = $this->getUser();
        $game_trades = $this->getDoctrine()->getRepository('AppBundle:TradeGamebooking')->findMyGameTrades($user);
        $gameday_trades = $this->getDoctrine()->getRepository('AppBundle:TradeGamedayBooking')->findMyGamedayTrades($user);
        return array(
            'game_trades' => $game_trades,
            'user' => $user,
            'gameday_trades' => $gameday_trades,
        );
    }

    public function countBookingsAction($gameday, $user) 
    {
        $em = $this->getDoctrine()->getManager();
        $gameday_users = $em->getRepository('AppBundle:GameDayBooking')->findUsers($gameday, $user);
        $game_users = $em->getRepository('AppBundle:GameBooking')->findBookedUsers($gameday, $user);
        $count_gameday = count($gameday_users);
        $count_game = count($game_users);
        if ($count_gameday == 0 && $count_game == 0) {
            return true;
        }
    }

    /**
     * @Route("/games/gameday_booking/{id}/update", name="update_gameday_booking")
     * @Template("/games/update_gameday_booking.html.twig")
     */
    public function updateGamedayBookingAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $gameday_booking = $em->getRepository('AppBundle:GameDayBooking')->find($id);
        $gameday = $gameday_booking->getGameDay();
        $type = $em->getRepository('AppBundle:Type')->find($gameday_booking->getType()->getId());
        $gameday_types = $em->getRepository('AppBundle:GameDayTypes')->findGamedayTypes($type, $gameday);
        $form = $this->createForm(new GameDayBookingType, $gameday_booking);
        $form->handleRequest($request);
        $count = $this->countBookingsAction($gameday, $gameday_booking->getUser());

        if ($form->isValid()) {
            
                $em->flush();
                if ($request->isXmlHttpRequest()) {
                    if ($count == 0) {
                        $response = array(
                            'success' => false,
                            'type' => $type->getName(),
                            'user' => $gameday_booking->getUser()->getCn(),
                            'gameday' => $gameday->getId(),
                            'message' => $gameday_booking->getUser()->getCn(). " är nu bokad fler än en gång det här matchtillfället.",
                    );
                        return new JsonResponse($response);
                    }
                    else {
                        $response = array(
                            'success' => true,
                            'type' => $type->getName(),
                            'user' => $gameday_booking->getUser()->getCn(),
                            'gameday' => $gameday->getId(),
                        );
                        return new JsonResponse($response);
                    }
                return $this->redirectToRoute('gametest_booking');
            }
            else {
                $response = array('message' => " är redan bokad det här matchtillfället.");
                return new JsonResponse($response);
            }
        }
       return array(
            'form' => $form->createView(), 
            'gameday_bookings' => $gameday_booking,
            'type' => $gameday_booking->getType(),
            'gameday_types' => $gameday_types,
            'gameday' => $gameday,
        );
    }

    /**
     * @Route("/games/admin_games/gameday_booking/{id}/{type_id}", name="gameday_booking")
     * @Template("/games/gameday_booking.html.twig")
     */
    public function newGamedayBookingAction(Request $request, $id, $type_id)
    {
        $gameday_booking = new GameDayBooking();
        $em = $this->getDoctrine()->getManager();
        $gameday = $em->getRepository('AppBundle:GameDay')->find($id);
        $type = $em->getRepository('AppBundle:Type')->find($type_id);
        $gameday_types = $em->getRepository('AppBundle:GameDayTypes')->findGamedayTypes($type, $gameday);
        $gameday_bookings = $em->getRepository('AppBundle:GameDayBooking')->findGamedayBookings($type, $gameday);
        $form = $this->createForm(new GameDayBookingType, $gameday_booking);
        $form->handleRequest($request);
        $count = $this->countBookingsAction($gameday, $gameday_booking->getUser());

        
            if ($form->isValid()) {
                $gameday_booking->setType($type);
                $gameday_booking->setGameday($gameday);
                $em->persist($gameday_booking);
                $em->flush();
                if ($request->isXmlHttpRequest()) {
                    if ($count == 0) {
                    $new_gameday_bookings = $em->getRepository('AppBundle:GameDayBooking')->findGamedayBookings($type, $gameday);
                    $response = array(
                        'success' => false,
                        'type' => $type->getName(),
                        'user' => $gameday_booking->getUser()->getCn(),
                        'gameday_type' => $gameday_types->getQuantity(),
                        'gameday_bookings' => $new_gameday_bookings,
                        'bookings' => $gameday_bookings,
                        'count' => $count,
                        'gameday_booking_id' => $gameday_booking->getId(),
                        'message' => $gameday_booking->getUser()->getCn(). " är nu bokad fler än en gång det här matchtillfället."
                        );
                    return new JsonResponse($response);
                }
                else {
                    $new_gameday_bookings = $em->getRepository('AppBundle:GameDayBooking')->findGamedayBookings($type, $gameday);
                    $response = array(
                        'success' => true,
                        'type' => $type->getName(),
                        'user' => $gameday_booking->getUser()->getCn(),
                        'gameday_type' => $gameday_types->getQuantity(),
                        'gameday_bookings' => $new_gameday_bookings,
                        'bookings' => $gameday_bookings,
                        'count' => $count,
                        );
                    return new JsonResponse($response);   
                }
                return $this->redirectToRoute('gametest_booking');
            }
        }
        
        return array(
            'gameday_booking' => $gameday_booking,
            'gameday' => $gameday,
            'form' => $form->createView(),
            'gameday_types' => $gameday_types,
            'gameday_bookings' => $gameday_bookings,
        );
    }
    
    /**
     * @Route("/games/admin_games/game_booking/{gameday_id}", name="game_booking")
     * @Template("/games/game_booking_test.html.twig")
     */
    public function newGameBookingAction(Request $request, $gameday_id)
    {
        $em = $this->getDoctrine()->getManager();
        $booking_repository = $em->getRepository('AppBundle:GameBooking');
        $gameday = $em->getRepository('AppBundle:GameDay')->find($gameday_id);
        $games = $em->getRepository('AppBundle:Games')->findGamesWithGameday($gameday_id);
        $game_booking = new GameBooking();
        $form = $form = $this->createFormBuilder($game_booking)
            ->add('game', 'entity', array(
                'class' => 'AppBundle:Games',
                'choice_label' => 'name',
                'placeholder' => '-',
                'label' => 'Match',
                'choices' => $games,
                ))
            ->add('user', 'entity', array(
                'class' => 'AppBundle:Users',
                'choice_label' => 'cn',
                'placeholder' => '-',
                'label' => 'Användare',
                ))
            ->add('type', 'entity', array(
                'class' => 'AppBundle:Type',
                'choice_label' => 'name',
                'label' => false,
                'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('t')
                    ->where('t.is_gamebooking = :true')
                    ->setParameter('true', true);
                    }
                ))
            ->add('save', 'submit', array('label' => 'Boka'))
            ->getForm();
        $form->handleRequest($request);
        $booking = $booking_repository->findBooking($game_booking->getGames(), $game_booking->getType());
        $count = $this->countBookingsAction($gameday, $game_booking->getUser());
        $count_booking = count($booking);

        if ($count_booking == 0) {
            if ($form->isValid()) {
                $game_booking->setGameDay($gameday);
                $em->persist($game_booking);
                $em->flush();
                if ($request->isXmlHttpRequest()) {
                    if ($count == 0) {
                        $response = array(
                            'success' => false, 
                            'user' => $game_booking->getUser()->getCn(),
                            'game_id' => $game_booking->getGames()->getId(),
                            //'type_id' => $type->getId(),
                            'gameday_id' => $gameday->getId(),
                            'gamebooking_id' => $game_booking->getId(),
                            'message' => $game_booking->getUser()->getCn(). " är nu bokad fler än en gång det här matchtillfället."
                        );
                        return new JsonResponse($response);
                    }
                    else {
                        $response = array(
                            'success' => true, 
                            'user' => $game_booking->getUser()->getCn(),
                            'game_id' => $game_booking->getGames()->getId(),
                            //'type_id' => $type->getId(),
                            'gamebooking_id' => $game_booking->getId(),
                            'gameday_id' => $gameday->getId(),
                        );
                        return new JsonResponse($response);
                    }
                }
                return $this->redirectToRoute('gametest_booking');
            }
        }
        elseif ($count_booking != 0) {
            $message = $game_booking->getType()->getName() . " är redan bokat för den här matchen.";
            $response = array('message' => $message);
            return new JsonResponse($response);
        }
        else {
            $message = "Användaren är redan bokad det här matchtillfället.";
            $response = array('message' => $message);
            return new JsonResponse($response);
        }
        return array(
            'form' => $form->createView(), 
            'gameday' => $gameday,
        );                              
    }

    /**
     * @Route("/games/admin_games/specific_game_booking/{gameday_id}/{game_id}/{type_id}", name="specific_game_booking")
     * @Template("/games/specific_game_booking.html.twig")
     */
    public function newSpecificGameBookingAction(Request $request, $gameday_id, $game_id, $type_id)
    {
        $em = $this->getDoctrine()->getManager();
        $booking_repository = $em->getRepository('AppBundle:GameBooking');
        $gameday = $em->getRepository('AppBundle:GameDay')->find($gameday_id);
        $game = $em->getRepository('AppBundle:Games')->find($game_id);
        $type = $em->getRepository('AppBundle:Type')->find($type_id);
        $game_booking = new GameBooking();
        $form = $form = $this->createFormBuilder($game_booking)

            ->add('user', 'entity', array(
                'class' => 'AppBundle:Users',
                'choice_label' => 'cn',
                'placeholder' => '-',
                'label' => 'Användare',
                ))

            ->add('save', 'submit', array('label' => 'Boka'))
            ->getForm();
        $form->handleRequest($request);
        $booking = $booking_repository->findBooking($game_booking->getGames(), $game_booking->getType());
        $count = $this->countBookingsAction($gameday, $game_booking->getUser());
        $count_booking = count($booking);

        if ($count_booking == 0) {
            if ($form->isValid()) {
                $game_booking->setGameDay($gameday);
                $game_booking->setType($type);
                $game_booking->setGames($game);
                $em->persist($game_booking);
                $em->flush();
                if ($request->isXmlHttpRequest()) {
                    if ($count == 0) {
                        $response = array(
                            'success' => false, 
                            'user' => $game_booking->getUser()->getCn(),
                            'game_id' => $game->getId(),
                            'type_id' => $type->getId(),
                            'gameday_id' => $gameday->getId(),
                            'gamebooking_id' => $game_booking->getId(),
                            'message' => $game_booking->getUser()->getCn(). " är nu bokad fler än en gång det här matchtillfället."
                        );
                        return new JsonResponse($response);
                    }
                    else {
                        $response = array(
                            'success' => true, 
                            'user' => $game_booking->getUser()->getCn(),
                            'game_id' => $game->getId(),
                            'type_id' => $type->getId(),
                            'gamebooking_id' => $game_booking->getId(),
                            'gameday_id' => $gameday->getId(),
                        );
                        return new JsonResponse($response);
                    }
                }
                
            }
        }
        elseif ($count_booking != 0) {
            $message = $game_booking->getType()->getName() . " är redan bokat för den här matchen.";
            $response = array('message' => $message);
            return new JsonResponse($response);
        }
        else {
            $message = "Användaren är redan bokad det här matchtillfället.";
            $response = array('message' => $message);
            return new JsonResponse($response);
        }
        return array(
            'form' => $form->createView(), 
            'gameday' => $gameday,
            'game' => $game,
            'type' => $type, 
        );      
    }

    /**
     * @Route("/games/admin_games/game_booking/{id}/update", name="update_game_booking")
     * @Template("/games/game_booking_update.html.twig")
     */
    public function updateGameBookingAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $booking_repository = $em->getRepository('AppBundle:GameBooking');
        $game_booking = $booking_repository->find($id);
        $gameday = $em->getRepository('AppBundle:GameDay')->find($game_booking->getGameDay());
        $form = $this->createForm(new UpdateGameBookingTestType, $game_booking);
        $form->handleRequest($request);
        $count = $this->countBookingsAction($gameday, $game_booking->getUser());

        if ($form->isValid()) {
            
                $em->flush();
                if ($request->isXmlHttpRequest()) {
                    if ($count == 0) {
                        $response = array(
                            'success' => false, 
                            'user' => $game_booking->getUser()->getCn(),
                            'game_id' => $game_booking->getGames()->getId(),
                            'message' => $game_booking->getUser()->getCn(). " är nu bokad fler än en gång det här matchtillfället.",
                    );
                    return new JsonResponse($response);
                }
                else {
                        $response = array(
                            'success' => true, 
                            'user' => $game_booking->getUser()->getCn(),
                            'game_id' => $game_booking->getGames()->getId(),
                            //'type_id' => $type->getId(),
                            'gamebooking_id' => $game_booking->getId(),
                            'gameday_id' => $gameday->getId(),
                        );
                        return new JsonResponse($response);
                    }
                return $this->redirectToRoute('update_game_booking');
            }
            else {
                $message = "Användaren är redan bokad det här matchtillfället.";
                $response = array('message' => $message, 'count' => $count);
                return new JsonResponse($response);
            }
        }
        return array(
            'form' => $form->createView(),
            'gameday' => $gameday,
            'booking' => $game_booking,

        );
    }

    /**
     * @Route("/game_booking/{id}/delete", name="delete_game_booking")
     * @Template()
     */
    public function deleteGameBookingAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $booking_repository = $em->getRepository('AppBundle:GameBooking');
        $game_booking = $booking_repository->find($id);
        $em->remove($game_booking);
        $em->flush();
        if ($request->isXmlHttpRequest()) {
            $response = array(
                'success' => true,
                'game_id' => $game_booking->getGames()->getId(),
                'type_id' => $game_booking->getType()->getId(),
                'gameday_id'=> $game_booking->getGameDay()->getId(),
            );
            return new JsonResponse($response);
        }
    }

    /**
     * @Route("/games/{id}/grade", name="game_grade")
     * @Template("/games/game_grade.html.twig")
     */
    public function updateGameGradeAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $game = $em->getRepository('AppBundle:Games')->find($id);
        $form = $this->createForm(new GameGradeType(), $game);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                $response = array('success' => true, 'game' => $game, 'grade' => $game->getGrade(),);
                return new JsonResponse($response);
            }
        }
        return array('form' => $form->createView(), 'game' => $game,);
    }

    /**
     * @Route("/games/{id}/grade/delete", name="delete_game_grade")
     * @Template()
     */
    public function deleteGameGradeAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $game = $em->getRepository('AppBundle:Games')->find($id);
        
        $game->setGrade(null);
        $em->flush();
        if ($request->isXmlHttpRequest()) {
            $response = array(
                'success' => true,
            );
            return new JsonResponse($response);
        }
    }

    /**
     * @Route("/gameday_booking/{id}/delete", name="delete_gameday_booking")
     * @Template()
     */
    public function deleteGameDayBookingAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $gameday_booking = $em->getRepository('AppBundle:GameDayBooking')->find($id);
        $em->remove($gameday_booking);
        $em->flush();
        
        if ($request->isXmlHttpRequest()) {
            $response = array(
                'success' => true,
            );
            return new JsonResponse($response);
        }
    }

}