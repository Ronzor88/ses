<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class ApprovedGamebookingTradeType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
        	->add('trade_approved', 'checkbox', array('label' => 'Godkänn'))
            ->add('save', 'submit', array('label' => 'Spara'));
    }

    public function getName()
    {
        return 'approved_gamebooking_trade';
    }

}