<?php



namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ProfileFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->remove('username')
            ->remove('current_password', 'password')

                ->add('title', 'text', array(
                    'label' => 'Titel', 
                    'translation_domain' => 'FOSUserBundle'))
                ->add('about', 'textarea', array(
                    'label' => 'Om', 
                    'translation_domain' => 'FOSUserBundle'))
                ->add('telephone', 'text', array(
                    'label' => 'Kontakt', 
                    'translation_domain' => 'FOSUserBundle'))
                ->add('imageFile', 'vich_image', array(
                    'required' => false,
                    'allow_delete' => false,
                    'download_link' => false,
                    ));
                


                

    }

    public function getParent()
    {
        return 'fos_user_profile';
    }

    public function getName()
    {
        return 'user_profile';
    }




}
