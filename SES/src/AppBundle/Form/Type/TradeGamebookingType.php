<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class TradeGamebookingType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('trade_message', 'text', array(
    		'label' => 'Meddelande:'))
            ->add('save', 'submit', array('label' => 'Skapa'));
    }

    public function getName()
    {
        return 'trade_gamebooking';
    }

}