<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class UpdateGameBookingTestType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('user', 'entity', array(
                'class' => 'AppBundle:Users',
                'choice_label' => 'cn',
                'placeholder' => '-',
                'label' => false
                ))
            ->add('type', 'entity', array(
                'class' => 'AppBundle:Type',
                'choice_label' => 'name',
                'placeholder' => '-',
                'label' => false,
                'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('t')
                    ->where('t.is_gamebooking = :true')
                    ->setParameter('true', true);
                    }
                ))

            ->add('save', 'submit');
    }

    public function getName()
    {
        return 'update_game_booking_test';
    }

}