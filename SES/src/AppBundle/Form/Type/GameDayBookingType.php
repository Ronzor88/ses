<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GameDayBookingType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('user', 'entity', array(
                'class' => 'AppBundle:Users',
                'choice_label' => 'cn',
                'placeholder' => '-',
                'label' => false,))

            ->add('save', 'submit');
    }

    public function getName()
    {
        return 'gameday_booking';
    }

}