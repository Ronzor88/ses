<?php



namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ChangeUserRoleType extends AbstractType
{

public function buildForm(FormBuilderInterface $builder, array $options)
{
    $permissions = array(
        'ROLE_USER'        => 'Vanlig användare',
        'ROLE_ADMIN'       => 'Admin',
        );

    $builder->add('user', 'entity', array(
                'class'    => 'AppBundle:Users',
                'property' => 'cn',
                'placeholder' => 'Användare',
                'label'    => 'Välj användare',))
        	
        	->add('role', 'choice', array(
                'placeholder' => 'Välj nivå',
        	'label'   => 'Behörighet',
            'choices' => $permissions,))
        	
        	->add('save','submit');
}


public function getName()
    {
        return 'ChangeUserRole';
    }

}