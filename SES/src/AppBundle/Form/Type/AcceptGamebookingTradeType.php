<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class AcceptGamebookingTradeType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('save', 'submit', array('label' => 'Ta passet'));
    }

    public function getName()
    {
        return 'accept_gamebooking_trade';
    }

}