<?php

namespace AppBundle\Form\Type;

use \Symfony\Component\Form\AbstractType;
use \Symfony\Component\Form\FormBuilderInterface;


class MessagesType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('headline', null, array('attr' => array('class' => 'headline'),'label' => 'Rubrik'))
            ->add('message', 'ckeditor', array(
                'required' => false,
                'attr' => array('class'=>'board_message','placeholder' => 'Skriv ditt meddelande här...'),
                'label' => 'Meddelande',
                'ui_color' => '#f4f2f0',
                'width'         => '100%',
                'height'        => '420',
            ));
    }

    public function getName()
    {
        return 'messages';

    }

}