<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GameBookingType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('user', 'entity', array(
                'class' => 'AppBundle:User',
                'choice_label' => 'name',
                'placeholder' => '-',
                'label' => false
                ))

            ->add('save', 'submit');
    }

    public function getName()
    {
        return 'game_booking';
    }

}