<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GamesTypes
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class GamesTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Games", inversedBy="gamestypes")
     **/
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="gamestypes")
     **/
    private $type;

    /**
     * Set game
     *
     * @param \AppBundle\Entity\Games $game
     * @return GamesTypes
     */
    public function setGame(\AppBundle\Entity\Games $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \AppBundle\Entity\Games 
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\Type $type
     * @return GamesTypes
     */
    public function setType(\AppBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set game_booking
     *
     * @param \AppBundle\Entity\GameBooking $gameBooking
     * @return GamesTypes
     */
    public function setGameBooking(\AppBundle\Entity\GameBooking $gameBooking = null)
    {
        $this->game_booking = $gameBooking;

        return $this;
    }

    /**
     * Get game_booking
     *
     * @return \AppBundle\Entity\GameBooking 
     */
    public function getGameBooking()
    {
        return $this->game_booking;
    }
}
