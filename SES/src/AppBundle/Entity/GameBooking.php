<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameBooking
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GameBookingRepository")
 */
class GameBooking
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="game_booking")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     **/
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="game_booking")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="GameDay")
     **/
    private $gameday;

    /**
     * @ORM\ManyToOne(targetEntity="Games", inversedBy="game_booking")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     **/
    private $game;

    /**
     * @ORM\OneToMany(targetEntity="TradeGamebooking", mappedBy="game_booking")
     **/
    private $trade;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\Users $user
     * @return GameBooking
     */
    public function setUser(\AppBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set gameday
     *
     * @param \AppBundle\Entity\GameDay $gameday
     * @return GameBooking
     */
    public function setGameday(\AppBundle\Entity\GameDay $gameday = null)
    {
        $this->gameday = $gameday;

        return $this;
    }

    /**
     * Get gameday
     *
     * @return \AppBundle\Entity\GameDay 
     */
    public function getGameday()
    {
        return $this->gameday;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\Type $type
     * @return GameBooking
     */
    public function setType(\AppBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set game
     *
     * @param \AppBundle\Entity\Games $game
     * @return GameBooking
     */
    public function setGames(\AppBundle\Entity\Games $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \AppBundle\Entity\Games
     */
    public function getGames()
    {
        return $this->game;
    }

    /**
     * Set game
     *
     * @param \AppBundle\Entity\Games $game
     * @return GameBooking
     */
    public function setGame(\AppBundle\Entity\Games $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \AppBundle\Entity\Games 
     */
    public function getGame()
    {
        return $this->game;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->trade = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add trade
     *
     * @param \AppBundle\Entity\TradeGamebooking $trade
     *
     * @return GameBooking
     */
    public function addTrade(\AppBundle\Entity\TradeGamebooking $trade)
    {
        $this->trade[] = $trade;

        return $this;
    }

    /**
     * Remove trade
     *
     * @param \AppBundle\Entity\TradeGamebooking $trade
     */
    public function removeTrade(\AppBundle\Entity\TradeGamebooking $trade)
    {
        $this->trade->removeElement($trade);
    }

    /**
     * Get trade
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrade()
    {
        return $this->trade;
    }
}
