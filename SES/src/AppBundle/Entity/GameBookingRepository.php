<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * GameBookingRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GameBookingRepository extends EntityRepository
{
   	public function findBookedUsers($gameday, $user)
    {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select('b')
            ->from('AppBundle:GameBooking', 'b')
            
            ->where('b.user = :user')
            ->andWhere('b.gameday = :gameday')
            ->setParameters(array('user' => $user, 'gameday' => $gameday))
            ->getQuery();
        return $qb->getQuery()->getArrayResult();
    }
    
    public function findBooking($game, $type)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('b')
            ->from('AppBundle:GameBooking', 'b')
            ->where('b.game = :game')
            ->andWhere('b.type = :type')        
            ->setParameters(array('game' => $game, 'type' => $type))
            ->getQuery();
        return $qb->getQuery()->getArrayResult();
    }

    public function findAllMyGameBookings($user)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('b')
            ->from('AppBundle:GameBooking', 'b')
            ->where('b.user = :user')
            ->setParameter('user', $user);
        return $qb->getQuery()->getResult();
    }

    public function findAllMyCommingGameBookings($user, $today)
        {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select('b')
                ->from('AppBundle:GameBooking', 'b')                
                ->where('b.user = :user')
                ->innerJoin('b.gameday', 'd')
                ->andwhere('d.date >= :today')
                ->orderBy('d.date', 'ASC')
                ->setParameters(array('user' => $user, 'today' => $today));
            return $qb->getQuery()->getResult();
        }

    public function findAllMyCompletedGameBookings($user, $today)
        {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select('b')
                ->from('AppBundle:GameBooking', 'b')                
                ->where('b.user = :user')
                ->innerJoin('b.gameday', 'd')
                ->andwhere('d.date < :today')
                ->orderBy('d.date', 'DESC')
                ->setParameters(array('user' => $user, 'today' => $today));
            return $qb->getQuery()->getResult();
        }

    public function findGameBookingByUserAndDateJson($user, $start_date, $end_date) 
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('g')
            ->from('AppBundle:GameBooking', 'g')
            ->join('g.gameday', 'd')
            ->where($qb->expr()->between(
                'd.date',
                ':from',
                ':to'
            ))
            ->andWhere('g.user = :user')
            ->orderBy('d.date', 'ASC')
            ->setParameters(array(
                'user' => $user,
                'from' => $start_date,
                'to' => $end_date,
            ));

        return $qb->getQuery()->getResult();
    }

    public function findGameBookingByUserAndDateCountJson($user, $start_date, $end_date) 
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('g')
            ->from('AppBundle:GameBooking', 'g')
            ->join('g.gameday', 'd')
            ->where($qb->expr()->between(
                'd.date',
                ':from',
                ':to'
            ))
            ->andWhere('g.user = :user')
            ->orderBy('g.type', 'ASC')
            ->setParameters(array(
                'user' => $user,
                'from' => $start_date,
                'to' => $end_date,
            ));
        return $qb->getQuery()->getResult();
    }
   
}
