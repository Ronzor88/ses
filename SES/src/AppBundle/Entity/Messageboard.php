<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Messageboard
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Messageboard
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="boardmessage", type="text")
     */
    private $boardmessage;

    /**
     * @ORM\ManyToOne(targetEntity="Users")
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set boardmessage
     *
     * @param string $boardmessage
     *
     * @return Messageboard
     */
    public function setBoardmessage($boardmessage)
    {
        $this->boardmessage = $boardmessage;

        return $this;
    }

    /**
     * Get boardmessage
     *
     * @return string
     */
    public function getBoardmessage()
    {
        return $this->boardmessage;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\Users $user
     *
     * @return Messageboard
     */
    public function setUser(\AppBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
