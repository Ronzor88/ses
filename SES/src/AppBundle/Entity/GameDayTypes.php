<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameDayTypes
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GameDayTypesRepository")
 */
class GameDayTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\ManyToOne(targetEntity="GameDay", inversedBy="gameday_types")
     **/
    private $gameday;

    /**
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="gameday_types")
     **/
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;



    /**
     * Set gameday
     *
     * @param \AppBundle\Entity\GameDay $gameday
     * @return GameDayTypes
     */
    public function setGameday(\AppBundle\Entity\GameDay $gameday = null)
    {
        $this->gameday = $gameday;

        return $this;
    }

    /**
     * Get gameday
     *
     * @return \AppBundle\Entity\GameDay 
     */
    public function getGameday()
    {
        return $this->gameday;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\Type $type
     * @return GameDayTypes
     */
    public function setType(\AppBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return GameDayTypes
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
