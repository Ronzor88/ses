<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameDay
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GameDayRepository")
 */
class GameDay
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity="Games", mappedBy="game_day")
     **/
    private $game;

    /**
     * @ORM\OneToMany(targetEntity="GameDayTypes", mappedBy="gameday")
     **/
    private $gameday_types;

    /**
     * @ORM\OneToMany(targetEntity="GameDayBooking", mappedBy="gameday")
     **/
    private $gameday_booking;

    /**
     * @var string
     *
     * @ORM\Column(name="league", type="string", length=255)
     */
    private $league;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->game = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add game
     *
     * @param \AppBundle\Entity\Games $game
     * @return GameDay
     */
    public function addGame(\AppBundle\Entity\Games $game)
    {
        $this->game[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param \AppBundle\Entity\Games $game
     */
    public function removeGame(\AppBundle\Entity\Games $game)
    {
        $this->game->removeElement($game);
    }

    /**
     * Get game
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return GameDay
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Add gameday_types
     *
     * @param \AppBundle\Entity\GameDayTypes $gamedayTypes
     * @return GameDay
     */
    public function addGamedayType(\AppBundle\Entity\GameDayTypes $gamedayTypes)
    {
        $this->gameday_types[] = $gamedayTypes;

        return $this;
    }

    /**
     * Remove gameday_types
     *
     * @param \AppBundle\Entity\GameDayTypes $gamedayTypes
     */
    public function removeGamedayType(\AppBundle\Entity\GameDayTypes $gamedayTypes)
    {
        $this->gameday_types->removeElement($gamedayTypes);
    }

    /**
     * Get gameday_types
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGamedayTypes()
    {
        return $this->gameday_types;
    }

    /**
     * Add gameday_booking
     *
     * @param \AppBundle\Entity\GameDayBooking $gamedayBooking
     * @return GameDay
     */
    public function addGamedayBooking(\AppBundle\Entity\GameDayBooking $gamedayBooking)
    {
        $this->gameday_booking[] = $gamedayBooking;

        return $this;
    }

    /**
     * Remove gameday_booking
     *
     * @param \AppBundle\Entity\GameDayBooking $gamedayBooking
     */
    public function removeGamedayBooking(\AppBundle\Entity\GameDayBooking $gamedayBooking)
    {
        $this->gameday_booking->removeElement($gamedayBooking);
    }

    /**
     * Get gameday_booking
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGamedayBooking()
    {
        return $this->gameday_booking;
    }

    /**
     * Set league
     *
     * @param string $league
     *
     * @return GameDay
     */
    public function setLeague($league)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return string
     */
    public function getLeague()
    {
        return $this->league;
    }
}
