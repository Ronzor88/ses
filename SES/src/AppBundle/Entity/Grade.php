<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Grade
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Grade
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="grade", type="integer", nullable=true)
     * @Assert\Range(
     *      min = 1,
     *      max = 5)
     */
    private $grade;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\OneToOne(targetEntity="Games", inversedBy="grade")
     * @ORM\JoinColumn(name="games_id", referencedColumnName="id")
     **/
    private $games;
    

    /**
     * Set Games
     *
     * @param \AppBundle\Entity\games $games
     * @return Grade
     */
    public function setGames(\AppBundle\Entity\Games $games = null)
    {
        $this->games = $games;

        return $this;
    }

    /**
     * Get Games
     *
     * @return \AppBundle\Entity\Games 
     */
    public function getGames()
    {
        return $this->games;
    }


    /**
     * Set grade
     *
     * @param integer $grade
     * @return Grade
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return integer 
     */
    public function getGrade()
    {
        return $this->grade;
    }
}
