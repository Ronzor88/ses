<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Type
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Type
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="GamesTypes", mappedBy="type")
     **/
    private $gamestypes;

    /**
     * @ORM\OneToMany(targetEntity="GameDayTypes", mappedBy="type")
     **/
    private $gameday_types;

    /**
     * @ORM\OneToMany(targetEntity="GameDayBooking", mappedBy="type")
     **/
    private $gameday_booking;

    /**
     * @ORM\OneToMany(targetEntity="GameBooking", mappedBy="type")
     **/
    private $game_booking;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_gamebooking", type="boolean")
     */
    private $is_gamebooking;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Type
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gamestypes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add gamestypes
     *
     * @param \AppBundle\Entity\GamesTypes $gamestypes
     * @return Type
     */
    public function addGamestype(\AppBundle\Entity\GamesTypes $gamestypes)
    {
        $this->gamestypes[] = $gamestypes;

        return $this;
    }

    /**
     * Remove gamestypes
     *
     * @param \AppBundle\Entity\GamesTypes $gamestypes
     */
    public function removeGamestype(\AppBundle\Entity\GamesTypes $gamestypes)
    {
        $this->gamestypes->removeElement($gamestypes);
    }

    /**
     * Get gamestypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGamestypes()
    {
        return $this->gamestypes;
    }

    /**
     * Add gameday_types
     *
     * @param \AppBundle\Entity\GameDayTypes $gamedayTypes
     * @return Type
     */
    public function addGamedayType(\AppBundle\Entity\GameDayTypes $gamedayTypes)
    {
        $this->gameday_types[] = $gamedayTypes;

        return $this;
    }

    /**
     * Remove gameday_types
     *
     * @param \AppBundle\Entity\GameDayTypes $gamedayTypes
     */
    public function removeGamedayType(\AppBundle\Entity\GameDayTypes $gamedayTypes)
    {
        $this->gameday_types->removeElement($gamedayTypes);
    }

    /**
     * Get gameday_types
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGamedayTypes()
    {
        return $this->gameday_types;
    }

    /**
     * Add gameday_booking
     *
     * @param \AppBundle\Entity\GameDayBooking $gamedayBooking
     * @return Type
     */
    public function addGamedayBooking(\AppBundle\Entity\GameDayBooking $gamedayBooking)
    {
        $this->gameday_booking[] = $gamedayBooking;

        return $this;
    }

    /**
     * Remove gameday_booking
     *
     * @param \AppBundle\Entity\GameDayBooking $gamedayBooking
     */
    public function removeGamedayBooking(\AppBundle\Entity\GameDayBooking $gamedayBooking)
    {
        $this->gameday_booking->removeElement($gamedayBooking);
    }

    /**
     * Get gameday_booking
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGamedayBooking()
    {
        return $this->gameday_booking;
    }

    /**
     * Add game_booking
     *
     * @param \AppBundle\Entity\GameBooking $gameBooking
     * @return Type
     */
    public function addGameBooking(\AppBundle\Entity\GameBooking $gameBooking)
    {
        $this->game_booking[] = $gameBooking;

        return $this;
    }

    /**
     * Remove game_booking
     *
     * @param \AppBundle\Entity\GameBooking $gameBooking
     */
    public function removeGameBooking(\AppBundle\Entity\GameBooking $gameBooking)
    {
        $this->game_booking->removeElement($gameBooking);
    }

    /**
     * Get game_booking
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGameBooking()
    {
        return $this->game_booking;
    }

    /**
     * Set is_gamebooking
     *
     * @param boolean $isGamebooking
     * @return Type
     */
    public function setIsGamebooking($isGamebooking)
    {
        $this->is_gamebooking = $isGamebooking;

        return $this;
    }

    /**
     * Get is_gamebooking
     *
     * @return boolean 
     */
    public function getIsGamebooking()
    {
        return $this->is_gamebooking;
    }
}
