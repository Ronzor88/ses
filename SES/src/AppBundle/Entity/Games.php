<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Games
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GamesRepository")
 */
class Games
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="GamesTypes", mappedBy="game")
     **/
    private $gamestypes;

    /**
     * @ORM\ManyToOne(targetEntity="GameDay", inversedBy="game")
     * @ORM\JoinColumn(name="game_day_id", referencedColumnName="id")
     **/
    private $game_day;

    /**
     * @ORM\OneToMany(targetEntity="GameBooking", mappedBy="game")
     **/
    private $game_booking;

    /**
     * @var integer
     *
     * @ORM\Column(name="grade", type="integer", nullable=true)
     * @Assert\Range(
     *      min = 1,
     *      max = 5)
     */
    private $grade;

    /**
     * @var \DateTime
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Games
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gamestypes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add gamestypes
     *
     * @param \AppBundle\Entity\GamesTypes $gamestypes
     * @return Games
     */
    public function addGamestype(\AppBundle\Entity\GamesTypes $gamestypes)
    {
        $this->gamestypes[] = $gamestypes;

        return $this;
    }

    /**
     * Remove gamestypes
     *
     * @param \AppBundle\Entity\GamesTypes $gamestypes
     */
    public function removeGamestype(\AppBundle\Entity\GamesTypes $gamestypes)
    {
        $this->gamestypes->removeElement($gamestypes);
    }

    /**
     * Get gamestypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGamestypes()
    {
        return $this->gamestypes;
    }

    /**
     * Set game_day
     *
     * @param \AppBundle\Entity\GameDay $gameDay
     * @return Games
     */
    public function setGameDay(\AppBundle\Entity\GameDay $gameDay = null)
    {
        $this->game_day = $gameDay;

        return $this;
    }

    /**
     * Get game_day
     *
     * @return \AppBundle\Entity\GameDay 
     */
    public function getGameDay()
    {
        return $this->game_day;
    }

    /**
     * Add gameday_booking
     *
     * @param \AppBundle\Entity\GameDayBooking $gamedayBooking
     * @return Games
     */
    public function addGamedayBooking(\AppBundle\Entity\GameDayBooking $gamedayBooking)
    {
        $this->gameday_booking[] = $gamedayBooking;

        return $this;
    }

    /**
     * Remove gameday_booking
     *
     * @param \AppBundle\Entity\GameDayBooking $gamedayBooking
     */
    public function removeGamedayBooking(\AppBundle\Entity\GameDayBooking $gamedayBooking)
    {
        $this->gameday_booking->removeElement($gamedayBooking);
    }

    /**
     * Get gameday_booking
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGamedayBooking()
    {
        return $this->gameday_booking;
    }

    /**
     * Add game_booking
     *
     * @param \AppBundle\Entity\GameBooking $gameBooking
     * @return Games
     */
    public function addGameBooking(\AppBundle\Entity\GameBooking $gameBooking)
    {
        $this->game_booking[] = $gameBooking;

        return $this;
    }

    /**
     * Remove game_booking
     *
     * @param \AppBundle\Entity\GameBooking $gameBooking
     */
    public function removeGameBooking(\AppBundle\Entity\GameBooking $gameBooking)
    {
        $this->game_booking->removeElement($gameBooking);
    }

    /**
     * Get game_booking
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGameBooking()
    {
        return $this->game_booking;
    }

    /**
     * Set grade
     *
     * @param integer $grade
     * @return Games
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return integer 
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return Games
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

}
