<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameDayBooking
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GameDayBookingRepository")
 */
class GameDayBooking
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="gameday_booking")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="GameDay", inversedBy="gameday_booking")
     * @ORM\JoinColumn(name="gameday_id", referencedColumnName="id")
     **/
    private $gameday;

    /**
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="gameday_booking")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     **/
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="TradeGamedayBooking", mappedBy="gameday_booking")
     **/
    private $trade;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\Users $user
     * @return GameDayBooking
     */
    public function setUser(\AppBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set game
     *
     * @param \AppBundle\Entity\Games $game
     * @return GameDayBooking
     */
    public function setGame(\AppBundle\Entity\Games $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \AppBundle\Entity\Games 
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\Type $type
     * @return GameDayBooking
     */
    public function setType(\AppBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set gameday
     *
     * @param \AppBundle\Entity\GameDay $gameday
     * @return GameDayBooking
     */
    public function setGameday(\AppBundle\Entity\GameDay $gameday = null)
    {
        $this->gameday = $gameday;

        return $this;
    }

    /**
     * Get gameday
     *
     * @return \AppBundle\Entity\GameDay 
     */
    public function getGameday()
    {
        return $this->gameday;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->trade = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add trade
     *
     * @param \AppBundle\Entity\TradeGamedaybooking $trade
     *
     * @return GameDayBooking
     */
    public function addTrade(\AppBundle\Entity\TradeGamedaybooking $trade)
    {
        $this->trade[] = $trade;

        return $this;
    }

    /**
     * Remove trade
     *
     * @param \AppBundle\Entity\TradeGamedaybooking $trade
     */
    public function removeTrade(\AppBundle\Entity\TradeGamedaybooking $trade)
    {
        $this->trade->removeElement($trade);
    }

    /**
     * Get trade
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrade()
    {
        return $this->trade;
    }
}
