<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TradeGamedayBooking
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TradeGamedayBookingRepository")
 */
class TradeGamedayBooking
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trade_message", type="text")
     */
    private $tradeMessage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="trade_created", type="datetime")
     */
    private $tradeCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="trade_approved_date", type="datetime", nullable=true)
     */
    private $tradeApprovedDate;

    /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="trade_user_id", referencedColumnName="id")
     *
     **/
    private $trade_user;

    /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="trade_accepted_user_id", referencedColumnName="id")
     *
     **/
    private $trade_accepted_user;

    /**
     * @ORM\ManyToOne(targetEntity="GameDayBooking", inversedBy="trade")
     * @ORM\JoinColumn(name="gamedaybooking_id", referencedColumnName="id")
     *
     **/
    private $gameday_booking;

    /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="trade_approved_user", referencedColumnName="id")
     *
     **/
    private $trade_approved_user;

    /**
     * @var boolean
     *
     * @ORM\Column(name="trade_approved", type="boolean")
     */
    private $tradeApproved;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tradeMessage
     *
     * @param string $tradeMessage
     *
     * @return TradeGamedayBooking
     */
    public function setTradeMessage($tradeMessage)
    {
        $this->tradeMessage = $tradeMessage;

        return $this;
    }

    /**
     * Get tradeMessage
     *
     * @return string
     */
    public function getTradeMessage()
    {
        return $this->tradeMessage;
    }

    /**
     * Set tradeCreated
     *
     * @param \DateTime $tradeCreated
     *
     * @return TradeGamedayBooking
     */
    public function setTradeCreated($tradeCreated)
    {
        $this->tradeCreated = $tradeCreated;

        return $this;
    }

    /**
     * Get tradeCreated
     *
     * @return \DateTime
     */
    public function getTradeCreated()
    {
        return $this->tradeCreated;
    }

    /**
     * Set tradeApprovedDate
     *
     * @param \DateTime $tradeApprovedDate
     *
     * @return TradeGamedayBooking
     */
    public function setTradeApprovedDate($tradeApprovedDate)
    {
        $this->tradeApprovedDate = $tradeApprovedDate;

        return $this;
    }

    /**
     * Get tradeApprovedDate
     *
     * @return \DateTime
     */
    public function getTradeApprovedDate()
    {
        return $this->tradeApprovedDate;
    }

    /**
     * Set tradeUser
     *
     * @param \AppBundle\Entity\Users $tradeUser
     *
     * @return TradeGamedayBooking
     */
    public function setTradeUser(\AppBundle\Entity\Users $tradeUser = null)
    {
        $this->trade_user = $tradeUser;

        return $this;
    }

    /**
     * Get tradeUser
     *
     * @return \AppBundle\Entity\Users
     */
    public function getTradeUser()
    {
        return $this->trade_user;
    }

    /**
     * Set tradeAcceptedUser
     *
     * @param \AppBundle\Entity\Users $tradeAcceptedUser
     *
     * @return TradeGamedayBooking
     */
    public function setTradeAcceptedUser(\AppBundle\Entity\Users $tradeAcceptedUser = null)
    {
        $this->trade_accepted_user = $tradeAcceptedUser;

        return $this;
    }

    /**
     * Get tradeAcceptedUser
     *
     * @return \AppBundle\Entity\Users
     */
    public function getTradeAcceptedUser()
    {
        return $this->trade_accepted_user;
    }

    /**
     * Set gamedayBooking
     *
     * @param \AppBundle\Entity\GameDayBooking $gamedayBooking
     *
     * @return TradeGamedayBooking
     */
    public function setGamedayBooking(\AppBundle\Entity\GameDayBooking $gamedayBooking = null)
    {
        $this->gameday_booking = $gamedayBooking;

        return $this;
    }

    /**
     * Get gamedayBooking
     *
     * @return \AppBundle\Entity\GameDayBooking
     */
    public function getGamedayBooking()
    {
        return $this->gameday_booking;
    }

    /**
     * Set tradeApprovedUser
     *
     * @param \AppBundle\Entity\Users $tradeApprovedUser
     *
     * @return TradeGamedayBooking
     */
    public function setTradeApprovedUser(\AppBundle\Entity\Users $tradeApprovedUser = null)
    {
        $this->trade_approved_user = $tradeApprovedUser;

        return $this;
    }

    /**
     * Get tradeApprovedUser
     *
     * @return \AppBundle\Entity\Users
     */
    public function getTradeApprovedUser()
    {
        return $this->trade_approved_user;
    }

    /**
     * Set tradeApproved
     *
     * @param boolean $tradeApproved
     *
     * @return TradeGamedayBooking
     */
    public function setTradeApproved($tradeApproved)
    {
        $this->tradeApproved = $tradeApproved;

        return $this;
    }

    /**
     * Get tradeApproved
     *
     * @return boolean
     */
    public function getTradeApproved()
    {
        return $this->tradeApproved;
    }
}
