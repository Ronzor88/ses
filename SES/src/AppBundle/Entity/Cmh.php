<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cmh
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CmhRepository")
 */
class Cmh
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\OneToOne(targetEntity="Games", inversedBy="cmh")
     * @ORM\JoinColumn(name="games_id", referencedColumnName="id")
     **/
    private $games;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="cmh")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $user;

    /**
     * Set games
     *
     * @param \AppBundle\Entity\Games $games
     * @return Cmh
     */
    public function setGames(\AppBundle\Entity\Games $games = null)
    {
        $this->games = $games;

        return $this;
    }

    /**
     * Get games
     *
     * @return \AppBundle\Entity\Games 
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Cmh
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
