<?php


namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use FR3D\LdapBundle\Model\LdapUserInterface;
use Doctrine\ORM\Mapping\AttributeOverrides;
use Doctrine\ORM\Mapping\AttributeOverride;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @Vich\Uploadable
  * @ORM\AttributeOverrides({
 *     @ORM\AttributeOverride(name="email",
 *          column=@ORM\Column(
 *              nullable = true
 *          )
 *      ),
 *     @ORM\AttributeOverride(name="emailCanonical",
 *          column=@ORM\Column(
 *              name = "email_canonical",
 *              nullable = true
 *          )
 *      ),
 *      @ORM\AttributeOverride(name="title",
 *          column=@ORM\Column(
 *              name = "title",
 *              nullable = true
 *          )
 *        ),
 *      @ORM\AttributeOverride(name="about",
 *          column=@ORM\Column(
 *              name = "about",
 *              nullable = true
 *          )
 *         ),
 *      @ORM\AttributeOverride(name="telephone",
 *          column=@ORM\Column(
 *              name = "telephone",
 *              nullable = true 
 *          )
 *      )
 * })
 */

class Users extends BaseUser implements LdapUserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Group")
     * @ORM\JoinTable(name="fos_user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;
    /**
     * @ORM\Column(name="dn", type="string", length=255)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $dn;
     /**
     * @ORM\Column(name="title", type="string", length=30)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $title;
    /**
     * @ORM\Column(name="about", type="string", length=255)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $about;
    /**
     * @ORM\Column(name="cn", type="string", length=255)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $cn;
    /**
     * @ORM\Column(name="telephone", type="string", length=40)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $telephone;

    /**
     * @ORM\OneToMany(targetEntity="GameBooking", mappedBy="user")
     **/
    private $game_booking;

    /**
     * @ORM\OneToMany(targetEntity="GameDayBooking", mappedBy="user")
     **/
    private $gameday_booking;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="imageName")
     * 
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;




    public function __construct()
    {
        parent::__construct();
        // your own logic
    }


    
    public function getDn()
    {
        return $this->dn;
    }

    public function setDn($dn)
    {
        $this->dn = $dn;

        return $this;
    }

     public function getCn()
    {
        return $this->cn;
    }

    public function setCn($cn)
    {
        $this->cn = $cn;

        return $this;
    }


    

    /**
     * Add gameBooking
     *
     * @param \AppBundle\Entity\GameBooking $gameBooking
     *
     * @return Users
     */
    public function addGameBooking(\AppBundle\Entity\GameBooking $gameBooking)
    {
        $this->game_booking[] = $gameBooking;

        return $this;
    }

    /**
     * Remove gameBooking
     *
     * @param \AppBundle\Entity\GameBooking $gameBooking
     */
    public function removeGameBooking(\AppBundle\Entity\GameBooking $gameBooking)
    {
        $this->game_booking->removeElement($gameBooking);
    }

    /**
     * Get gameBooking
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGameBooking()
    {
        return $this->game_booking;
    }

    /**
     * Add gamedayBooking
     *
     * @param \AppBundle\Entity\GameDayBooking $gamedayBooking
     *
     * @return Users
     */
    public function addGamedayBooking(\AppBundle\Entity\GameDayBooking $gamedayBooking)
    {
        $this->gameday_booking[] = $gamedayBooking;

        return $this;
    }

    /**
     * Remove gamedayBooking
     *
     * @param \AppBundle\Entity\GameDayBooking $gamedayBooking
     */
    public function removeGamedayBooking(\AppBundle\Entity\GameDayBooking $gamedayBooking)
    {
        $this->gameday_booking->removeElement($gamedayBooking);
    }

    /**
     * Get gamedayBooking
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGamedayBooking()
    {
        return $this->gameday_booking;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Users
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set about
     *
     * @param string $about
     *
     * @return Users
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }
    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Users
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    
    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Users
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

  

    /**
     * Set imageName
     *
     * @param string $imageName
     *
     * @return Users
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    
 


}
