<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TradeGamebooking
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TradeGameBookingRepository")
 */
class TradeGamebooking
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="trade_approved", type="boolean")
     */
    private $tradeApproved;

    /**
     * @var string
     *
     * @ORM\Column(name="trade_message", type="text")
     */
    private $tradeMessage;

    /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="trade_user_id", referencedColumnName="id")
     *
     **/
    private $trade_user;

    /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="trade_accepted_user_id", referencedColumnName="id")
     *
     **/
    private $trade_accepted_user;

    /**
     * @ORM\ManyToOne(targetEntity="GameBooking", inversedBy="trade")
     * @ORM\JoinColumn(name="gamebooking_id", referencedColumnName="id")
     *
     **/
    private $game_booking;

    /**
     * @var \DateTime
     * @ORM\Column(name="trade_created", type="datetime")
     */
    private $tradeCreatedDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="trade_approved_date", type="datetime", nullable=true)
     */
    private $tradeApprovedDate;

    /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="trade_approved_user", referencedColumnName="id")
     *
     **/
    private $trade_approved_user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tradeApproved
     *
     * @param boolean $tradeApproved
     *
     * @return Trade
     */
    public function setTradeApproved($tradeApproved)
    {
        $this->tradeApproved = $tradeApproved;

        return $this;
    }

    /**
     * Get tradeApproved
     *
     * @return boolean
     */
    public function getTradeApproved()
    {
        return $this->tradeApproved;
    }

    /**
     * Set tradeMessage
     *
     * @param string $tradeMessage
     *
     * @return Trade
     */
    public function setTradeMessage($tradeMessage)
    {
        $this->tradeMessage = $tradeMessage;

        return $this;
    }

    /**
     * Get tradeMessage
     *
     * @return string
     */
    public function getTradeMessage()
    {
        return $this->tradeMessage;
    }

    /**
     * Set tradeUser
     *
     * @param \AppBundle\Entity\Users $tradeUser
     *
     * @return TradeGamebooking
     */
    public function setTradeUser(\AppBundle\Entity\Users $tradeUser = null)
    {
        $this->trade_user = $tradeUser;

        return $this;
    }

    /**
     * Get tradeUser
     *
     * @return \AppBundle\Entity\Users
     */
    public function getTradeUser()
    {
        return $this->trade_user;
    }

    /**
     * Set tradeAcceptedUser
     *
     * @param \AppBundle\Entity\Users $tradeAcceptedUser
     *
     * @return TradeGamebooking
     */
    public function setTradeAcceptedUser(\AppBundle\Entity\Users $tradeAcceptedUser = null)
    {
        $this->trade_accepted_user = $tradeAcceptedUser;

        return $this;
    }

    /**
     * Get tradeAcceptedUser
     *
     * @return \AppBundle\Entity\Users
     */
    public function getTradeAcceptedUser()
    {
        return $this->trade_accepted_user;
    }

    /**
     * Set gameBooking
     *
     * @param \AppBundle\Entity\GameBooking $gameBooking
     *
     * @return TradeGamebooking
     */
    public function setGameBooking(\AppBundle\Entity\GameBooking $gameBooking = null)
    {
        $this->game_booking = $gameBooking;

        return $this;
    }

    /**
     * Get gameBooking
     *
     * @return \AppBundle\Entity\GameBooking
     */
    public function getGameBooking()
    {
        return $this->game_booking;
    }

    /**
     * Set tradeCreatedDate
     *
     * @param \DateTime $tradeCreatedDate
     *
     * @return TradeGamebooking
     */
    public function setTradeCreatedDate($tradeCreatedDate)
    {
        $this->tradeCreatedDate = $tradeCreatedDate;

        return $this;
    }

    /**
     * Get tradeCreatedDate
     *
     * @return \DateTime
     */
    public function getTradeCreatedDate()
    {
        return $this->tradeCreatedDate;
    }

    /**
     * Set tradeApprovedDate
     *
     * @param \DateTime $tradeApprovedDate
     *
     * @return TradeGamebooking
     */
    public function setTradeApprovedDate($tradeApprovedDate)
    {
        $this->tradeApprovedDate = $tradeApprovedDate;

        return $this;
    }

    /**
     * Get tradeApprovedDate
     *
     * @return \DateTime
     */
    public function getTradeApprovedDate()
    {
        return $this->tradeApprovedDate;
    }

    /**
     * Set tradeApprovedUser
     *
     * @param \AppBundle\Entity\Users $tradeApprovedUser
     *
     * @return TradeGamebooking
     */
    public function setTradeApprovedUser(\AppBundle\Entity\Users $tradeApprovedUser = null)
    {
        $this->trade_approved_user = $tradeApprovedUser;

        return $this;
    }

    /**
     * Get tradeApprovedUser
     *
     * @return \AppBundle\Entity\Users
     */
    public function getTradeApprovedUser()
    {
        return $this->trade_approved_user;
    }
}
