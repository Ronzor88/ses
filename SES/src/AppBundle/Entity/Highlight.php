<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Highlight
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\HighlightRepository")
 */
class Highlight
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\OneToOne(targetEntity="Games", inversedBy="highlight")
     * @ORM\JoinColumn(name="games_id", referencedColumnName="id")
     *
     **/
    private $games;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="highlight")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     **/
    private $user;


    /**
     * Set Games
     *
     * @param \AppBundle\Entity\Games $games
     * @return Highlight
     */
    public function setGames(\AppBundle\Entity\Games $games = null)
    {
        $this->games = $games;

        return $this;
    }

    /**
     * Get Games
     *
     * @return \AppBundle\Entity\Games 
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Highlight
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
