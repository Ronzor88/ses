function gameBooking(gameday_id) 
{
	
	$('#booking_fail').hide();
	$('#booking_warning').hide();
	$('#booking_check').hide();
	$('#booking_cross').hide();
	$('#booking_footer').hide();
	var $gameday_id = gameday_id;
	$('form').submit(function (e) {
		e.preventDefault();
		var $data = $(this).serialize();
		$('#booking_fail').hide();

		$.ajax({
			type: 'POST',
			url: '/games/admin_games/game_booking/'+$gameday_id,
			data: $data
		})
		.done(function (result) {
			var $before_href = '<a data-toggle="modal" data-target="#gameModal" href="'
			var $after_href = '">';
			if (result.success) {
				
				var $href = '/games/admin_games/game_booking/'+result.gamebooking_id+'/update';
				$('#booking_warning').hide();
				$('#booking_footer').hide();
				$('#booking_check').hide();
				$('#booking_check').fadeIn();
				$('#booking_fail').hide();
				$('#booking_cross').hide();
				console.log(result);
				document.getElementById('game_td-'+result.game_id).innerHTML = $before_href+$href+$after_href+result.user+'</a>';
				$("#game_td-"+result.game_id).addClass("update_success");
				setTimeout(function() {
					$("#game_td-"+result.game_id).removeClass("update_success");
					}, 5000);
			}
			else if (result.success === false) {
				$('#booking_footer').show();
				var $href = '/games/admin_games/game_booking/'+result.gamebooking_id+'/update';
				$('#booking_check').hide();
				$('#booking_check').fadeIn();
				$('#booking_fail').hide();
				$('#booking_cross').hide();
				$('#booking_warning').show();
				$('#booking_footer').show();
				console.log(result);
				document.getElementById('booking_warning').innerHTML = '<i class="fa fa-exclamation-triangle"></i> '+result.message;
				document.getElementById('game_td-'+result.game_id).innerHTML = $before_href+$href+$after_href+result.user+'</a>';
				$("#game_td-"+result.game_id).addClass("update_success");
				setTimeout(function() {
					$("#game_td-"+result.game_id).removeClass("update_success");
					}, 5000);

				var $before_href = '<a data-toggle="modal" data-target="#gameModal" href="'
				var $href = '/games/admin_games/specific_game_booking/';
				var $after_href = '">';
				var $add_user = '<i class="fa fa-user-plus"></i></a>';
					
				var $booking_id = result.gamebooking_id;
				$('#undo_booking_button').click(function() {
		            $.ajax({
		                type: 'POST',
		                url: '/game_booking/'+$booking_id+'/delete',
		            }).done(function (result) {
		            	console.log(result);
		            	if (result.success) {
		            		console.log($before_href+$href+$after_href+$add_user);
		            		$('#gameday_booking_table').show();
		            		$('#booking_check').hide();
		            		$('#booking_fail').hide();
							$('#booking_cross').hide();
							$('#booking_warning').hide();
							$('#booking_footer').hide();
		            		document.getElementById('game_td-'+result.game_id).innerHTML = $before_href+$href+result.gameday_id+'/'+result.game_id+'/'+result.type_id+$after_href+$add_user;
		            	}		            	
		            }).fail(function() {
		            	console.log("Fel");
		            })
				});
			}
			else {
				$('#booking_warning').hide();
				$('#booking_footer').hide();
				$('#booking_cross').hide();
				$('#booking_check').hide();
				$('#booking_cross').fadeIn();
				$('#booking_fail').show();
				document.getElementById('booking_fail').innerHTML = result.message;
                }
			
		})
		.fail(function (error) {
			alert("FEL");
			console.log(error);
		})
	});
    $(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
        $(e.target).removeData("bs.modal").find(".modal-content").empty();
    });
};

function specificGameBooking(gameday_id, game_id, type_id) 
{
	var $gameday_id = gameday_id;
	var $game_id = game_id;
	var $type_id = type_id;
	$('#booking_fail').hide();
	$('#booking_check').hide();
	$('#booking_cross').hide();
	$('#booking_warning').hide();
	$('#booking_footer').hide();

	$('form').submit(function (e) {

		e.preventDefault();
		var $data = $(this).serialize();
		$('#booking_fail').hide();

		$.ajax({
			type: 'POST',
			url: '/games/admin_games/specific_game_booking/'+$gameday_id+'/'+$game_id+'/'+$type_id,
			data: $data
		})
		.done(function (result) {
			var $before_href = '<a data-toggle="modal" data-target="#gameModal" href="'
			var $after_href = '">';
			var $href = '/games/admin_games/game_booking/'+result.gamebooking_id+'/update';
			if (result.success === true) {
				$('#gameModal').modal('hide');
				$('#booking_check').hide();
				$('#booking_check').fadeIn();
				$('#booking_fail').hide();
				$('#booking_cross').hide();
				console.log(result);
				document.getElementById('game_td-'+result.game_id).innerHTML = $before_href+$href+$after_href+result.user+'</a>';
				$("#game_td-"+result.game_id).addClass("update_success");
				setTimeout(function() {
					$("#game_td-"+result.game_id).removeClass("update_success");
					}, 5000);
			}
			else if (result.success === false) {
				$('#gameday_booking_table').hide();
				$('#booking_check').hide();
				$('#booking_check').fadeIn();
				$('#booking_fail').hide();
				$('#booking_cross').hide();
				$('#booking_warning').show();
				$('#booking_footer').show();
				console.log(result);
				document.getElementById('booking_warning').innerHTML += ' '+result.message;
				document.getElementById('game_td-'+result.game_id).innerHTML = $before_href+$href+$after_href+result.user+'</a>';
				$("#game_td-"+result.game_id).addClass("update_success");
				setTimeout(function() {
					$("#game_td-"+result.game_id).removeClass("update_success");
					}, 5000);

				var $before_href = '<a data-toggle="modal" data-target="#gameModal" href="'
				var $href = '/games/admin_games/specific_game_booking/';
				var $after_href = '">';
				var $add_user = '<i class="fa fa-user-plus"></i></a>';
					
				var $booking_id = result.gamebooking_id;
				$('#undo_booking_button').click(function() {
		            $.ajax({
		                type: 'POST',
		                url: '/game_booking/'+$booking_id+'/delete',
		            }).done(function (result) {
		            	console.log(result);
		            	if (result.success) {
		            		console.log($before_href+$href+$after_href+$add_user);
		            		$('#gameday_booking_table').show();
		            		$('#booking_check').hide();
		            		$('#booking_fail').hide();
							$('#booking_cross').hide();
							$('#booking_warning').hide();
							$('#booking_footer').hide();
		            		document.getElementById('game_td-'+result.game_id).innerHTML = $before_href+$href+result.gameday_id+'/'+result.game_id+'/'+result.type_id+$after_href+$add_user;
		            	}
		            	
		            }).fail(function() {
		            	console.log("Fel");
		            })
				});
			}
			else {

				$('#booking_cross').hide();
				$('#booking_check').hide();
				$('#booking_cross').fadeIn();
				$('#booking_fail').show();
				document.getElementById('booking_fail').innerHTML = result.message;
                }
			
		})
		.fail(function (error) {
			alert("FEL");
			console.log(error);
		})
	});
    $(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
        $(e.target).removeData("bs.modal").find(".modal-content").empty();
        $('a').removeClass('highlight_active');
    });
};


