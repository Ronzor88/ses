function gamedayBookingUpdate(gameday_booking_id) 
{
	var $gameday_booking_id = gameday_booking_id;
	var $before_href = '<a data-toggle="modal" data-target="#gameModal" href="'
	var $href = '/games/gameday_booking/'+$gameday_booking_id+'/update';
	var $after_href = '">';
	$('#booking_fail').hide();
	$('#booking_cross').hide();
	$('#booking_check').hide();
	$('#booking_warning').hide();

	$('form').submit(function (e) {
		e.preventDefault();
		var $data = $(this).serialize();
		$('#booking_fail').hide();

		$.ajax({
			type: 'POST',
			url: '/games/gameday_booking/'+$gameday_booking_id+'/update',
			data: $data
		})
		.done(function (result) {
			console.log(result);
			if (result.success) {
				$('#booking_warning').hide();
				$('#booking_fail').hide();
				$('#booking_cross').hide();
				$('#booking_check').hide();
				$('#booking_check').fadeIn();
				document.getElementById('gameday_th-'+gameday_booking_id).innerHTML = result.type+': '+$before_href+$href+$after_href+result.user+'</a>';
				$("#gameday_th-"+gameday_booking_id).addClass("update_success");
				setTimeout(function() {
					$("#gameday_th-"+gameday_booking_id).removeClass("update_success");
					}, 5000);
			}
			else if (result.success == false) {
				$('#booking_warning').show();
				$('#booking_fail').hide();
				$('#booking_cross').hide();
				$('#booking_check').hide();
				$('#booking_check').fadeIn();
				document.getElementById('booking_warning').innerHTML = '<i class="fa fa-exclamation-triangle"></i> '+result.message;
				document.getElementById('gameday_th-'+gameday_booking_id).innerHTML = result.type+': '+$before_href+$href+$after_href+result.user+'</a>';
				$("#gameday_th-"+gameday_booking_id).addClass("update_success");
				setTimeout(function() {
					$("#gameday_th-"+gameday_booking_id).removeClass("update_success");
					}, 5000);
			}
			else {
				var $name = $('#gameday_booking_user').find(":selected").text();
				$('#booking_fail').hide();
				$('#booking_cross').hide();
				$('#booking_check').hide();
				$('#booking_cross').fadeIn();
				$('#booking_fail').show();
				document.getElementById('booking_fail').innerHTML = $name+result.message;	
			}
		})
		.fail(function (error) {
			console.log(error);
		})
	});
    $(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
        $(e.target).removeData("bs.modal").find(".modal-content").empty();
    });
};

function gamedayBookingDelete(gameday_booking_id)
{
	var $gameday_booking_id = gameday_booking_id;
	$('#booking_delete').hide();	
	
	$('#delete_booking_button').click(function() {
            $.ajax({
                type: 'POST',
                url: '/gameday_booking/'+$gameday_booking_id+'/delete',
            }).done(function (result) {
            	console.log(result);
            	if (result.success) {
            		$('#gameModal').modal('hide');
            		$('#gameday_booking_table').hide();
            		$('#booking_delete').show();
            		$('.modal-footer').hide();
            		document.getElementById('gameday_th-'+$gameday_booking_id).innerHTML = "";
            	}
            	
            }).fail(function() {
            	console.log("Fel");
            })
});
}



