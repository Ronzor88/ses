function newGamebookingTrade(gamebooking_id) 
{
	$('#trade_success').hide();
	$gamebooking_id = gamebooking_id
	$('form').submit(function (e) {
		e.preventDefault();
		var $data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: '/trade/new/'+$gamebooking_id,
			data: $data,
		})
		.done(function (result) {
			console.log(result);
			if (result.success) {
				$('#trade_content').hide();
				$('#trade_success').fadeIn();
				//document.getElementById('grade_td-'+$game_id).innerHTML = '<a href="#">'+result.grade+'</a>'
				//$("#grade_td-"+game_id).addClass("update_success");
			}
						
		})
		.fail(function (error) {
			console.log(error);
		})
	});

	$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};

function acceptGamebookingTrade(trade_id) 
{
	$('#spinner').hide();
	$('#trade_success').hide();
	$trade_id = trade_id
	$('form').submit(function (e) {
		$('#trade_content').hide();
		$('#spinner').show();
		e.preventDefault();
		var $data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: '/trade/accept/'+$trade_id,
			data: $data,
		})
		.done(function (result) {

			console.log(result);
			if (result.success) {
				$('#spinner').hide();
				$('#trade_success').fadeIn();
				
				//document.getElementById('grade_td-'+$game_id).innerHTML = '<a href="#">'+result.grade+'</a>'
				//$("#grade_td-"+game_id).addClass("update_success");
				//setTimeout(function() {
				//	$("#grade_td-"+game_id).removeClass("update_success");
				//	}, 5000);;
			}
						
		})
		.fail(function (error) {
			console.log(error);
		})
	});

	$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};

function approveGamebookingTrade(trade_id) 
{
	$('#trade_success').hide();
	$trade_id = trade_id
	$('form').submit(function (e) {
		e.preventDefault();
		var $data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: '/trade/approve/'+$trade_id,
			data: $data,
		})
		.done(function (result) {
			console.log(result);
			if (result.success) {
				$('#trade_content').hide();
				$('#trade_success').fadeIn();
				
				//document.getElementById('grade_td-'+$game_id).innerHTML = '<a href="#">'+result.grade+'</a>'
				//$("#grade_td-"+game_id).addClass("update_success");
				//setTimeout(function() {
				//	$("#grade_td-"+game_id).removeClass("update_success");
				//	}, 5000);;
			}
						
		})
		.fail(function (error) {
			console.log(error);
		})
	});

	$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};

function newGamedayBookingTrade(gameday_booking_id) 
{
	$('#trade_success').hide();
	$gameday_booking_id = gameday_booking_id
	$('form').submit(function (e) {
		e.preventDefault();
		var $data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: '/trade/new_gameday/'+$gameday_booking_id,
			data: $data,
		})
		.done(function (result) {
			console.log(result);
			if (result.success) {
				$('#trade_content').hide();
				$('#trade_success').fadeIn();
				
				//document.getElementById('grade_td-'+$game_id).innerHTML = '<a href="#">'+result.grade+'</a>'
				//$("#grade_td-"+game_id).addClass("update_success");
				//setTimeout(function() {
				//	$("#grade_td-"+game_id).removeClass("update_success");
				//	}, 5000);;

			}
						
		})
		.fail(function (error) {
			console.log(error);
		})
	});

	$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};

function acceptGamedayBookingTrade(trade_id) 
{
	$('#spinner').hide();
	$('#trade_success').hide();
	$trade_id = trade_id
	$('form').submit(function (e) {
		$('#trade_content').hide();
		$('#spinner').show();
		e.preventDefault();
		var $data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: '/trade/accept_gameday/'+$trade_id,
			data: $data,
		})
		.done(function (result) {
			console.log(result);
			if (result.success) {
				$('#spinner').hide();
				$('#trade_content').hide();
				$('#trade_success').fadeIn();
				
				//document.getElementById('grade_td-'+$game_id).innerHTML = '<a href="#">'+result.grade+'</a>'
				//$("#grade_td-"+game_id).addClass("update_success");
				//setTimeout(function() {
				//	$("#grade_td-"+game_id).removeClass("update_success");
				//	}, 5000);;
			}
						
		})
		.fail(function (error) {
			console.log(error);
		})
	});

	$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};

function approveGamedayBookingTrade(trade_id) 
{
	$('#trade_success').hide();
	$trade_id = trade_id
	$('form').submit(function (e) {
		e.preventDefault();
		var $data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: '/trade/approve_gameday/'+$trade_id,
			data: $data,
		})
		.done(function (result) {
			console.log(result);
			if (result.success) {
				$('#trade_content').hide();
				$('#trade_success').fadeIn();
				
				//document.getElementById('grade_td-'+$game_id).innerHTML = '<a href="#">'+result.grade+'</a>'
				//$("#grade_td-"+game_id).addClass("update_success");
				//setTimeout(function() {
				//	$("#grade_td-"+game_id).removeClass("update_success");
				//	}, 5000);;
			}
						
		})
		.fail(function (error) {
			console.log(error);
		})
	});

	$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};

function removeGameBookingTrade(trade_id) 
{
	$('#trade_success').hide();
	$trade_id = trade_id
	$('#remove_trade_button').click(function() {
		$.ajax({
			type: 'POST',
			url: '/trade/game/'+$trade_id+'/remove',
		})
		.done(function (result) {
			console.log(result);
			if (result.success) {
				$('#trade_content').hide();
				$('#trade_success').fadeIn();
			}
						
		})
		.fail(function (error) {
			console.log(error);
		})
	});

	$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};

function removeGamedayBookingTrade(trade_id) 
{
	$('#trade_success').hide();
	$trade_id = trade_id
	$('#remove_trade_button').click(function() {
		$.ajax({
			type: 'POST',
			url: '/trade/gameday/'+$trade_id+'/remove',
		})
		.done(function (result) {
			console.log(result);
			if (result.success) {
				$('#trade_content').hide();
				$('#trade_success').fadeIn();
			}
						
		})
		.fail(function (error) {
			console.log(error);
		})
	});

	$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};

function undoGameBookingTrade(trade_id) 
{
	$('#trade_success').hide();
	$trade_id = trade_id
	$('#remove_trade_button').click(function() {
		$.ajax({
			type: 'POST',
			url: '/trade/game/undo/'+$trade_id+'/save',
		})
		.done(function (result) {
			console.log(result);
			if (result.success) {
				$('#trade_content').hide();
				$('#trade_success').fadeIn();
			}
						
		})
		.fail(function (error) {
			console.log(error);
		})
	});

	$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};

function undoGamedayBookingTrade(trade_id) 
{
	$('#trade_success').hide();
	$trade_id = trade_id
	$('#remove_trade_button').click(function() {
		$.ajax({
			type: 'POST',
			url: '/trade/gameday/undo/'+$trade_id+'/save',
		})
		.done(function (result) {
			console.log(result);
			if (result.success) {
				$('#trade_content').hide();
				$('#trade_success').fadeIn();
			}
						
		})
		.fail(function (error) {
			console.log(error);
		})
	});

	$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};


