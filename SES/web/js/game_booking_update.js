$('a').click(function() {
    $(this).addClass('highlight_active');
});

function updateGameBooking(booking_id) 
{
	var $before_href = '<a data-toggle="modal" data-target="#gameModal" href="'
	var $href = '/games/admin_games/game_booking/'+booking_id+'/update';
	var $after_href = '">';
	var $booking_id = booking_id;
	$('#booking_fail').hide();
	$('#booking_check').hide();
	$('#booking_cross').hide();
	$('#booking_warning').hide();
	$('#undo_booking_button').hide();

	$('form').submit(function (e) {
		e.preventDefault();
		var $data = $(this).serialize();
		$('#booking_fail').hide();

		$.ajax({
			type: 'POST',
			url: '/games/admin_games/game_booking/'+booking_id+'/update',
			data: $data
		})
		.done(function (result) {
			if (result.success) {
				$('#delete_booking_button').show();
				
				$('#booking_warning').hide();
				$('#booking_fail').hide();
				$('#booking_cross').hide();
				$('#booking_check').hide();
				$('#booking_check').fadeIn();
				console.log(result);
				document.getElementById('game_td-'+result.game_id).innerHTML = $before_href+$href+$after_href+result.user+'</a>';
				$("#game_td-"+result.game_id).addClass("update_success");
				setTimeout(function() {
					$("#game_td-"+result.game_id).removeClass("update_success");
					}, 5000);
			}
			else if (result.success === false) {
				$('#booking_warning').show();
				
				document.getElementById('booking_warning').innerHTML = '<i class="fa fa-exclamation-triangle"></i> '+result.message;
				console.log(result);
				document.getElementById('game_td-'+result.game_id).innerHTML = $before_href+$href+$after_href+result.user+'</a>';
				$("#game_td-"+result.game_id).addClass("update_success");
				setTimeout(function() {
					$("#game_td-"+result.game_id).removeClass("update_success");
					}, 5000);
			}
			else {
				$('#booking_cross').hide();
				$('#booking_check').hide();
				$('#booking_cross').fadeIn();
				$('#booking_fail').show();
				document.getElementById('booking_fail').innerHTML = result.message;
                }
			
		})
		.fail(function (error) {
			console.log(error);
		})
	});
    $(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
        $(e.target).removeData("bs.modal").find(".modal-content").empty();
        $('a').removeClass('highlight_active');
    });
};

function deleteGameBooking(booking_id)
{
	var $before_href = '<a data-toggle="modal" data-target="#gameModal" href="'
	var $href = '/games/admin_games/specific_game_booking/';
	var $after_href = '">';
	var $add_user = '<i class="fa fa-user-plus"></i></a>';
	$('#booking_delete').hide();	
	var $booking_id = booking_id;
	$('#delete_booking_button').click(function() {
            $.ajax({
                type: 'POST',
                url: '/game_booking/'+$booking_id+'/delete',
            }).done(function (result) {
            	console.log(result);
            	if (result.success) {
            		$('#gameModal').modal('hide');
            		console.log($before_href+$href+$after_href+$add_user);
            		$('#gameday_booking_table').hide();
            		$('#booking_delete').show();
            		$('#delete_booking_button').hide();
            		document.getElementById('game_td-'+result.game_id).innerHTML = $before_href+$href+result.gameday_id+'/'+result.game_id+'/'+result.type_id+$after_href+$add_user;
            	}
            	
            }).fail(function() {
            	console.log("Fel");
            })
});
}

