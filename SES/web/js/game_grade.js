function gameGrade(game_id) 
{
	$('#booking_check').hide();
	$game_id = game_id
	$('form').submit(function (e) {
		e.preventDefault();
		var $data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: '/games/'+$game_id+'/grade',
			data: $data,
		})
		.done(function (result) {
			console.log(result);
			if (result.success) {
				
				$('#booking_check').show();
				document.getElementById('grade_td-'+$game_id).innerHTML = '<a href="#">'+result.grade+'</a>'
				$("#grade_td-"+game_id).addClass("update_success");
				setTimeout(function() {
					$("#grade_td-"+game_id).removeClass("update_success");
					}, 5000);;
				$('#myGamesModal').modal('hide');
			}
						
		})
		.fail(function (error) {
			console.log(error);
		})
	});

	$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};

function deleteGameGrade(game_id)
{
		
	var $game_id = game_id;
	$('#delete_booking_button').click(function() {
            $.ajax({
                type: 'POST',
                url: '/games/'+$game_id+'/grade/delete',
            }).done(function (result) {
            	console.log(result);
            	if (result.success) {
            		$('#gameModal').modal('hide');
            		document.getElementById('grade_td-'+$game_id).innerHTML = '<a href="#">-</a>';
            		$('#myGamesModal').modal('hide');
            	}
            }).fail(function() {
            	console.log("Fel");
            })
});
		$(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
    	$(e.target).removeData("bs.modal").find(".modal-content").empty();
    	location.reload(false);
    });
};

