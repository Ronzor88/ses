function gamedayBooking(gameday_id, type_id) 
{
	var $gameday_id = gameday_id;
	var $type_id = type_id;
	$('#booking_fail').hide();
	$('#booking_check').hide();
	$('#booking_cross').hide();
	$('#booking_complete').hide();
	$('#booking_warning').hide();
	$('#booking_footer').hide();

	$('form').submit(function (e) {
		e.preventDefault();
		var $data = $(this).serialize();
		$('#booking_fail').hide();

		$.ajax({
			type: 'POST',
			url: '/games/admin_games/gameday_booking/'+$gameday_id+'/'+$type_id,
			data: $data
		})
		.done(function (result) {
			
			if (result.success) {
				console.log(result);
				$('#booking_footer').hide();
				$('#booking_fail').hide();
				$('#booking_check').hide();
				$('#booking_cross').hide();
				$('#booking_check').fadeIn();
				$('#booking_warning').hide();
				console.log(result);
				document.getElementById('gameday_tr-'+$gameday_id).innerHTML += '<th>'+result.type+': ' +'<a href="#">'+result.user+'</a>'+'</th>';
			
			}
			else if (result.success == false) {
				$('#booking_footer').show();
				$('#booking_warning').show();
				$gameday_booking_id = result.gameday_booking_id;
				$('#booking_fail').hide();
				$('#booking_check').hide();
				$('#booking_cross').hide();
				$('#booking_check').fadeIn();
				console.log(result);
				document.getElementById('booking_warning').innerHTML = '<i class="fa fa-exclamation-triangle"></i> '+result.message;
				document.getElementById('gameday_tr-'+$gameday_id).innerHTML += '<th id="gameday_th-'+$gameday_booking_id+'">'+result.type+': ' +'<a href="#">'+result.user+'</a>'+'</th>';
					$('#undo_booking_button').click(function() {
            $.ajax({
                type: 'POST',
                url: '/gameday_booking/'+$gameday_booking_id+'/delete',
            }).done(function (result) {
            	console.log(result);
            	if (result.success) {
            		$('#booking_warning').hide();
            		$('#booking_check').hide();
            		$('#booking_delete').show();
            		$('#booking_footer').hide();
            		document.getElementById('gameday_th-'+$gameday_booking_id).innerHTML = "";
            	}
            	
            }).fail(function() {
            	console.log("Fel");
            })
});
			}
			else {
				$('#booking_check').hide();
				$('#booking_cross').hide();
				console.log(result)
				$('#booking_cross').fadeIn();
				$booking_user = $("#gameday_booking_user option:selected").text();
				$('#booking_fail').show();
				document.getElementById('booking_fail').innerHTML = $booking_user+' är redan bokad det här matchtillfället.';
				
                }
			
		})
		.fail(function (error) {
			console.log(error);
		})
	});
    $(document).on("hidden.bs.modal", ".modal:not(.local-modal)", function (e) {
        $(e.target).removeData("bs.modal").find(".modal-content").empty();
    });
};



