$(function() {
    $('#div_gameday').hide();
    $('#div_game').hide();
    $('#spin').hide();
    $('.no_results').hide();
    $('#div_total').hide();
});

$(function() {
    var $user = $('#report_user');
    var $start_date_month = $('#report_start_date_month');
    var $start_date_day = $('#report_start_date_day');
    var $start_date_year = $('#report_start_date_year');
    var $end_date_month = $('#report_end_date_month');
    var $end_date_day = $('#report_end_date_day');
    var $end_date_year = $('#report_end_date_year');
    var $gameday = $('#show_gameday');
    var $cmhs = $('#show_cmh');
    var $game = $('#show_game');
    var $total_table = $('#show_total');
    var $gameday_length = 0;

        
$('#report_save').on('click', function(event) {
    $("#div_gameday").hide();
    $("#div_game").hide();
    $('.no_results').hide();
    $('#div_total').hide();
    $('#spin').show();
        event.preventDefault();
        
        $.ajax({
            type: 'GET',
            url: '/report/gameday/count',
            data: {
                user: $user.val(),
                start_date: $start_date_year.val() + '-' + $start_date_month.val() + '-' + $start_date_day.val(),
                end_date: $end_date_year.val() + '-' + $end_date_month.val() + '-' + $end_date_day.val() + ' ' + '23:00:00',
            },
            success: function (result) {

                $("#show_game").empty();
                $("#show_gameday").empty();
                if (result.length == 0) {
                    $("#div_gameday").hide();
                }
                else {
                    $("#div_gameday").show();
                    $("#report_gameday_count").empty();
                    var $type = "";
                    var $type_count = 0;
                    var $result_length = 0;
                    $gameday_length = result.length;
                    $result_length = result.length;
                    $.each(result, function (i, result) {
                        if (result.type != $type) {
                            console.log($type+$type_count);
                            if ($type_count != 0) {
                                document.getElementById('report_gameday_count').innerHTML += ' '+$type+'('+$type_count+')';
                            }
                            $type_count = 0;
                            $type = result.type;
                            $type_count++;
                        }
                        else {
                            $type_count++;
                        }
                        if (i == $result_length-1) {
                            console.log($type+$type_count)
                            document.getElementById('report_gameday_count').innerHTML += ' '+$type+'('+$type_count+')';
                        }    
                    });
                }
            },
            error: function () {
                console.log('error');
            }
        });
        
        $.ajax({
            type: 'GET',
            url: '/report/gameday',
            data: {
                user: $user.val(),
                start_date: $start_date_year.val() + '-' + $start_date_month.val() + '-' + $start_date_day.val(),
                end_date: $end_date_year.val() + '-' + $end_date_month.val() + '-' + $end_date_day.val() + ' ' + '23:00:00',
            },
            success: function (result) {
                   $("#show_gameday").empty();                    
                    $gameday.append('<th>Datum</th><th>Typ</th>');
                    $result_length = result.length;
                    $.each(result, function (i, result) {
                        $gameday.append('<tr><td>' + result.date + '</td><td>' + result.type + '</td></tr>');
                    });
            },
            error: function () {
                console.log('error');
            }
        });
 
        $.ajax({
            type: 'GET',
            url: '/report/game/count',
            data: {
                user: $user.val(),
                start_date: $start_date_year.val() + '-' + $start_date_month.val() + '-' + $start_date_day.val(),
                end_date: $end_date_year.val() + '-' + $end_date_month.val() + '-' + $end_date_day.val() + ' ' + '23:00:00',
            },
            success: function (result) {
                if ($gameday_length == 0 && result.length == 0) {
                    console.log("test");
                    $('.no_results').show();
                    $('#spin').hide();
                }
                if (result.length == 0) {
                    $("#div_game").hide();
                }
                
                else {
                    $("#div_game").show();
                    $("#report_game_count").empty();
                    var $type = "";
                    var $type_count = 0;
                    var $result_length = 0;
                    $game_length = result.length;
                    $result_length = result.length;
                    $.each(result, function (i, result) {
                        if (result.type != $type) {
                            console.log($type+$type_count);
                            if ($type_count != 0) {
                                document.getElementById('report_game_count').innerHTML += ' '+$type+'('+$type_count+')';
                            }
                            $type_count = 0;
                            $type = result.type;
                            $type_count++;
                        }
                        else {
                            $type_count++;
                        }
                        if (i == $result_length-1) {
                            console.log($type+$type_count)
                            document.getElementById('report_game_count').innerHTML += ' '+$type+'('+$type_count+')';
                        }    
                    });
                }
            },
            error: function () {
                console.log('error');
            }
        });
        
        $.ajax({
            type: 'GET',
            url: '/report/game',
            data: {
                user: $user.val(),
                start_date: $start_date_year.val() + '-' + $start_date_month.val() + '-' + $start_date_day.val(),
                end_date: $end_date_year.val() + '-' + $end_date_month.val() + '-' + $end_date_day.val() + ' ' + '23:00:00',
            },
            success: function (result) {
                    if (result.length == 0) {
                        $("#div_game").hide();
                    }
                    else {                     
                    $game.append('<th>Datum</th><th>Typ</th>');
                    $result_length = result.length;
                    $.each(result, function (i, result) {
                        $game.append('<tr><td>' + result.date + '</td><td>' + result.type + '</td></tr>');
                    });
                    $('#spin').hide();
                }
                
            },
            error: function () {
                console.log('error');
            }

        });
        
    });

});


